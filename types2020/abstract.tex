% easychair.tex,v 3.5 2017/03/15

\documentclass[a4paper]{easychair}
%\documentclass[EPiC]{easychair}
%\documentclass[EPiCempty]{easychair}
%\documentclass[debug]{easychair}
%\documentclass[verbose]{easychair}
%\documentclass[notimes]{easychair}
%\documentclass[withtimes]{easychair}
%\documentclass[a4paper]{easychair}
%\documentclass[letterpaper]{easychair}

\usepackage{doc}
\usepackage{amssymb}

% use this if you have a long article and want to create an index
% \usepackage{makeidx}

% In order to save space or manage large tables or figures in a
% landcape-like text, you can use the rotating and pdflscape
% packages. Uncomment the desired from the below.
%
% \usepackage{rotating}
% \usepackage{pdflscape}

%\makeindex

%% Front Matter
%%
% Regular title as in the article class.
%
\title{Metatheoretic proofs internally to presheaf categories%
\thanks{The first author was supported by the European Union, co-financed by the European Social Fund (EFOP-3.6.3-VEKOP-16-2017-00002). The second author was supported by the ÚNKP-19-4 New National Excellence Program of the Ministry for Innovation and Technology and by the Bolyai Fellowship of the Hungarian Academy of Sciences. The third author was supposed by USAF grant FA9550-16-1-0029.}}

% Authors are joined by \and. Their affiliations are given by \inst, which indexes
% into the list defined using \institute
%
\author{
  Rafaël Bocquet\inst{1}
  \and
  Ambrus Kaposi\inst{1}
  \and
  Christian Sattler\inst{2}
}

% Institutes for affiliations are also joined by \and,
\institute{
  Eötvös Loránd University,
  Budapest, Hungary\\
  \email{bocquet@inf.elte.hu} and \email{akaposi@inf.elte.hu} \\
  University of Nottingham, United Kingdom \\
  \email{sattler.christian@gmail.com}
}

%  \authorrunning{} has to be set for the shorter version of the authors' names;
% otherwise a warning will be rendered in the running heads. When processed by
% EasyChair, this command is mandatory: a document without \authorrunning
% will be rejected by EasyChair

\authorrunning{
  R. Bocquet,
  A. Kaposi,
  C. Sattler
}

% \titlerunning{} has to be set to either the main title or its shorter
% version for the running heads. When processed by
% EasyChair, this command is mandatory: a document without \titlerunning
% will be rejected by EasyChair
\titlerunning{Metatheoretic proofs internally to presheaf categories}

\newcommand{\Ty}{\mathsf{Ty}}
\newcommand{\Tm}{\mathsf{Tm}}
\newcommand{\Var}{\mathsf{Var}}
\newcommand{\var}{\mathsf{var}}
\newcommand{\SSet}{\mathsf{Set}}
\newcommand{\Ob}{\mathsf{Ob}}

\DeclareFontFamily{U}{min}{}
\DeclareFontShape{U}{min}{m}{n}{<-> udmj30}{}
\newcommand{\yo}{\text{\usefont{U}{min}{m}{n}\symbol{'210}}}

\newcommand{\CC}{\mathcal{C}}
\renewcommand{\SS}{\mathcal{S}}
\newcommand{\RR}{\mathcal{R}}
\newcommand{\GG}{\mathcal{G}}

\begin{document}

\maketitle

%\begin{abstract}
%\end{abstract}

% The table of contents below is added for your convenience. Please do not use
% the table of contents if you are preparing your paper for publication in the
% EPiC Series or Kalpa Publications series

%\section{To mention}
%
%Processing in EasyChair - number of pages.
%
%Examples of how EasyChair processes papers. Caveats (replacement of EC
%class, errors).

%------------------------------------------------------------------------------
\paragraph{Introduction}
Proofs of the metatheoretic properties of dependent type theories and other typed logics and languages, such as proofs of canonicity, normalization, gluing, parametricity or various translations between theories, involve complicated inductions over the syntax of the theory. We present a way to structure such proofs by working in the internal type-theoretic languages of suitable presheaf categories.

\paragraph{Internal models of type theory}

We use categories with families (CwFs) \cite{Dybjer95InternalTT, CwFUSD} equipped with additional structure as the models of our type theories. They consist of a category $\CC$, equipped with presheaves of types and terms, objects representing the empty context and context extensions, along with natural transformations for each type-theoretic operation and equations between them. Most of the additional structure on $\CC$ can concisely be described in the type-theoretic internal language of the presheaf category $\widehat{\CC}$. This observation is used in some of the existing general definitions of type theories \cite{CapriottiThesis, GeneralFrameworkTT}. For example, the presheaves of types and terms, and the $\mathbb{N}$ and $\Pi$ type formers can be specified in the internal language of $\widehat{\CC}$ as follows:
\[
  \begin{array}{lll}
    \Ty &:& \SSet \\
    \Tm &:& \Ty \to \SSet \\
    \mathbb{N} &:& \Ty \\
    \Pi &:& (A : \Ty) \to (B : \Tm\ A \to \Ty) \to \Ty
  \end{array} \]
The type $(\Tm\ A \to \Ty)$ of the argument $B$ of $\Pi$ is a presheaf function type: using the internal language of presheaf categories is a way to interpret higher-order abstract syntax (HOAS). $\Pi$-types are usually given externally by a map $\Pi_{\Gamma} : (A : \Ty_{\Gamma}) \to \Ty_{\Gamma \rhd A} \to \Ty_{\Gamma}$, natural in $\Gamma$, but the properties of the context extension operation $(- \rhd -)$ imply that the internal and external definitions are equivalent.

Only the empty context and the context extension operations can not directly be described internally, unless we use the interpretation of \emph{crisp type theory} \cite{ShulmanSpatialTypeTheory, LicataOPS} in $\widehat{\CC}$ and its comonadic modality $\flat$.

%------------------------------------------------------------------------------

\paragraph{Internal dependent models}

The generalized algebraic presentation of CwFs automatically provides an initial model $\SS$ satisfying an induction principle: there is a dependent section from $\SS$ to any dependent model over $\SS$. The definition of dependent model can be derived mechanically from the QIIT-signature presenting the type theory \cite{ConstructingQIITs}. By applying a similar transformation to the internal definition of models, we define a notion of dependent model internal to $\widehat{\SS}$:
\[
\begin{array}{lll}
  \Ty^{\bullet} &:& \Ty \to \SSet \\
  \Tm^{\bullet} &:& \{A\} (A^{\bullet} : \Ty^{\bullet}\ A) (a : \Tm\ A) \to \SSet \\
  \mathbb{N}^{\bullet} &:& \Ty^{\bullet}\ \mathbb{N} \\
  \Pi^{\bullet} &:& \{A\} (A^{\bullet} : \Ty^{\bullet}\ A) \{B\} (B^{\bullet} : \{a\} (a^{\bullet} : \Tm^{\bullet}\ A^{\bullet}\ a) \to \Ty^{\bullet}\ (B\ a)) \to \Ty^{\bullet}\ (\Pi\ A\ B) \\
\end{array} \]

Internal and external dependent models do not exactly correspond to each other, but we can still reconstruct an external dependent model from any internal one, and then obtain, externally, a dependent section of the reconstructed external model.

A proof of canonicity based on logical predicates can be given as an internal dependent model in the internal language of $\widehat{\SS}$:
\[
\begin{array}{lll}
  \Ty^{\bullet}\ A &:\equiv& \Tm\ A \to \SSet \\
  \Tm^{\bullet}\ A^{\bullet} &:\equiv& \lambda (a : \Tm\ A) \mapsto A^{\bullet}\ a \\
  \mathbb{N}^{\bullet} &:\equiv& \lambda (n : \Tm\ \mathbb{N}) \mapsto (m : \mathbf{N}) \times (n = \mathsf{suc}^{m}\ \mathsf{zero}) \\
  \Pi^{\bullet}\ A^{\bullet}\ B^{\bullet} &:\equiv& \lambda (f : \Tm\ (\Pi\ A\ B)) \mapsto (a : \Tm\ A)(a^{\bullet} : A^{\bullet}\ a) \to B^{\bullet}\ (\mathsf{app}\ f\ a) \\
\end{array} \]

%------------------------------------------------------------------------------

\paragraph{Internal induction principles}

The presheaf category $\widehat{\SS}$ is not a nice setting for more complicated proofs, such as normalization proofs: it forces all of our constructions to be stable under all substitutions, but normal forms are only stable under renamings. To fix this, we change the base category. For normalization, we work in the presheaf category $\widehat{\GG}$ over the comma category $\GG = (\SS \downarrow F)$, where $F : \RR \to \SS$ is the CwF morphism from the CwF of renamings $\RR$ to $\SS$. % More generally, we can work over the oplax limit of an arbitrary inverse diagram of CwFs.

The presheaf category $\widehat{\GG}$ has many good properties: the CwF structures of $\RR$ and $\SS$ can faithfully be transported over $\GG$, and the CwF morphism $F : \RR \to \SS$ can also faithfully be encoded. Furthermore, we can distinguish in $\widehat{\GG}$ the presheaves that come from $\widehat{\RR}$ or $\widehat{\SS}$; and call them $\RR$-discrete or $\SS$-discrete presheaves. Type-theoretically, they are accessible reflective subuniverses of the universe of all presheaves. Moreover, the $\RR$-/$\SS$-discrete presheaves can be identified with the discrete types arising from interpretations of spatial type theory \cite{ShulmanSpatialTypeTheory} in $\widehat{\GG}$. In particular, we have an adjoint pair of modalities $(\Box \dashv \lozenge)$, where the comonadic modality $\Box$ classifies the $\RR$-discrete presheaves. This means that we can reuse the theory of modalities developed in \cite{ModalitiesHoTT, ShulmanSpatialTypeTheory} in this setting.

We can then use the $\Box$ modality to define \emph{$F$-relative internal dependent models}, which encode inductions over the syntax whose results are only stable under renamings. We can define and prove an induction principle for $F$-relative dependent models.

A recent normalization proof by Coquand \cite{CoquandNbE} can be translated to this framework by defining a suitable $F$-relative dependent model. We can prove normalization and the decidability of equality for types and terms fully internally, without ever working explicitly with contexts, substitutions or renamings in the proof.

\paragraph{{Agda formalization\footnote{\url{https://gitlab.com/RafaelBocquet/internal_metatheory/tree/master/Agda}}}}

We have formalized in Agda internal proofs of canonicity and normalization for a reasonably large dependent type theory (including $\Pi$-types with the $\eta$ rule, booleans, natural numbers, identity types and a universe closed under the other type formers).

%------------------------------------------------------------------------------

\bibliographystyle{plain}
%\bibliographystyle{alpha}
%\bibliographystyle{unsrt}
%\bibliographystyle{abbrv}
\bibliography{abstract}

%------------------------------------------------------------------------------
% Index
%\printindex

%------------------------------------------------------------------------------
\end{document}

