\section{Dependent Right Adjoints and morphisms of models}\label{sec:dras}

In this section, we review the syntax and semantics of dependent right adjoints (DRAs)~\cite{DRAs}, and use the syntax of the dependent right adjoint $(F_{!} \dashv \modcolor{F^{\ast}})$ to give an internal encoding of the notion of morphism of models of $\Th_{\Pi,\BoolTy}$.
Multimodal Type Theory is only needed for some of the proofs and constructions performed in the appendix.
% For this purpose we use a variant of Multimodal Type Theory~\cite{MTT} that we describe in \cref{sec:mtt}.

\subsection{Dependent Right Adjoints}

Fix a functor $F : \CC \to \CD$.
The precomposition functor $F^{\ast} : \CPsh^{\CD} \to \CPsh^{\CC}$ has both a left adjoint $F_{!} : \CPsh^{\CC} \to \CPsh^{\CD}$ and a right adjoint $F_{\ast} : \CPsh^{\CC} \to \CPsh^{\CD}$.
The functors $F^{\ast}$ and $F_{\ast}$ are not only right adjoints of $F_{!}$ and $F^{\ast}$, they are dependent right adjoints, which means that they admit actions on the types and terms of the presheaf models $\CPsh^{\CC}$ and $\CPsh^{\CD}$ that interact with the left adjoints.
We distinguish the functor $F^{\ast}$ from the dependent right adjoint $\modcolor{F^{\ast}}$ by using different colors.
The dependent adjunction $(F^{\ast} \dashv \modcolor{F_{\ast}})$ is constructed in~\cite[Lemma 8.2]{MTT}, whereas $(F_{!} \dashv \modcolor{F^{\ast}})$ is constructed in~\cite[Lemma 2.1.4]{MTTtechreport}.
We recall their constructions in \cref{sec:dra_constructions}.

We focus on the description of the dependent right adjoint $\modcolor{F^{\ast}}$ as a syntactic and type-theoretic operation.
For every presheaf $X : \CPsh^{\CC}$ and dependent presheaf $A$ over $F_{!}\ X$, we have a dependent presheaf $\modcolor{F^{\ast}}\ A$ over $X$, such that elements of $A$ over $F_{!}\ X$ are in natural bijection with elements of $\modcolor{F^{\ast}}\ A$ over $X$.

This is analogous to the definition of $\Pi$-types: given a presheaf $X : \CPsh^{\CC}$, a dependent presheaf $Y(x)$ over the $(x : X)$ and a dependent presheaf $Z(x, y)$ over $(x : X, y : Y(x))$, the $\Pi$-type $(y : Y(x)) \to Z(x, y)$ over $(x : X)$ is characterized by the fact that its elements are in natural bijection with the elements of $Z(x,y)$ over $(x : X, y : Y(x))$.

Following this intuition, we use a similar syntax for $\Pi$-types and modalities.
We view the left adjoint $F_{!}$ as an operation on the contexts of the presheaf model $\CPsh^{\CC}$. If $(x : X)$ is a context of this presheaf model, we write $(x : X, \mmod{F^{\ast}})$ instead of $F_{!}\ X$.
Given a dependent presheaf $Y(x, \mmod{F^{\ast}})$\footnote{Here the notation $Y(x, \mmod{F^{\ast}})$ is an informal way to keep track of the fact that $Y$ is dependent over the context $(x : X, \mmod{F^{\ast}})$.} over $(x : X, \mmod{F^{\ast}})$, we write $(\mmod{F^{\ast}} \to Y(x, \mmod{F^{\ast}}))$ instead of $\modcolor{F^{\ast}}\ Y$.

We write the components of the bijection between elements of $Y(x, \mmod{F^{\ast}})$ over $(x : X, \mmod{F^{\ast}})$ and elements of $(\mmod{F^{\ast}} \to Y(x, \mmod{F^{\ast}}))$ over $(x : X)$ similarly to applications and $\lambda$-abstractions.
If $y(x, \mmod{F^{\ast}})$ is an element of $Y(x, \mmod{F^{\ast}})$ over $(x : X, \mmod{F^{\ast}})$, we write $(\lambda\ \mmod{F^{\ast}} \mapsto y(x, \mmod{F^{\ast}}))$ for the corresponding element of $(\mmod{F^{\ast}} \to Y(x, \mmod{F^{\ast}}))$.
Conversely, given an element $f(x)$ of $(\mmod{F^{\ast}} \to Y(x, \mmod{F^{\ast}}))$ over $(x : X)$, we write $f(x)\ \mmod{F^{\ast}}$ for the corresponding element of $Y(x, \mmod{F^{\ast}})$.
There is a $\beta$-rule $(\lambda\ \mmod{F^{\ast}} \mapsto y(x, \mmod{F^{\ast}}))\ \mmod{F^{\ast}} = y(x, \mmod{F^{\ast}})$ and an $\eta$-rule $(\lambda\ \mmod{F^{\ast}} \mapsto f(x)\ \mmod{F^{\ast}}) = f(x)$.

We may define elements of modal types by pattern matching. For instance, we may write $f(x)\ \mmod{F^{\ast}} \triangleq y(x,\mmod{F^{\ast}})$ to define $f(x)$ as the unique element satisfying the equation $f(x)\ \mmod{F^{\ast}} = y(x,\mmod{F^{\ast}})$, that is $f(x) \triangleq \lambda\ \mmod{F^{\ast}} \mapsto y(x,\mmod{F^{\ast}})$.

The operation $(\mmod{F^{\ast}} \to -)$ is a modality that enables interactions between the two presheaf models $\CPsh^{\CC}$ and $\CPsh^{\CD}$.
The symbols $\mmod{F^{\ast}}$ and $\mmod{F^{\ast}}$ and their places in the terms have been chosen to make keeping track of the modes of subterms as easy as possible.
For both symbols $\mmod{F^{\ast}}$ and $\mmod{F^{\ast}}$, the part of the term that is left of the symbol is at mode $\CPsh^{\CC}$, while the part that is right of the symbol is at mode $\CPsh^{\CD}$.
The type formers $(\mmod{F^{\ast}} \to -)$ and the term former $(\lambda\ \mmod{F^{\ast}} \mapsto -)$ go from the mode $\CPsh^{\CD}$ to $\CPsh^{\CC}$, whereas the term former $(-\ \mmod{F^{\ast}})$ goes from the mode $\CPsh^{\CC}$ to the mode $\CPsh^{\CD}$.

\subsection{Modalities are applicative functors}

As a first demonstration of the syntax of modalities, we equip the modality $(\mmod{F^{\ast}} \to -)$ with the structure of an \emph{applicative functor}~\cite{ApplicativeFunctors}, defined analogously to the \emph{reader monad} $(A \to -)$.
This structure is given by an operation
\begin{alignat*}{3}
  & (\_{} \circledast \_{}) && :{ } && \forall A\ B\ (f : \mmod{F^{\ast}} \to (a : A\ \mmod{F^{\ast}}) \to B\ \mmod{F^{\ast}}\ a) (a : \mmod{F^{\ast}} \to A\ \mmod{F^{\ast}}) \\
  &&&&& \to (\mmod{F^{\ast}} \to B\ \mmod{F^{\ast}}\ (a\ \mmod{F^{\ast}})) \\
  & f \circledast a && \triangleq{ } && \lambda\ \mmod{F^{\ast}} \mapsto (f\ \mmod{F^{\ast}})\ (a\ \mmod{F^{\ast}})
\end{alignat*}

This provides a concise notation to apply functions under the modality.
If $f$ is an $n$-ary function under the modality, and $a_{1},\dotsc,a_{n}$ are arguments under the modality, we can write the application $f \circledast a_{1} \circledast \cdots \circledast a_{n}$ instead of $(\lambda\ \mmod{F^{\ast}} \mapsto (f\ \mmod{F^{\ast}})\ (a_{1}\ \mmod{F^{\ast}})\ \cdots\ (a_{n}\ \mmod{F^{\ast}}))$.

When $f$ is a global function of the presheaf model $\CPsh^{\CD}$, we write $f \circleddollar a_{1} \circledast \cdots \circledast a_{n}$ instead of $(\lambda\ \mmod{F^{\ast}} \mapsto f) \circledast a_{1} \circledast \dotsc \circledast a_{n}$.

\subsection{Preservation of context extensions}

The last component that we need for an internal definition of morphism of models of $\Th_{\Pi}$ is an internal way to describe preservation of extended contexts of locally representable presheaves.
The preservation of context extensions can be expressed without assuming that the extended contexts actually exist, \ie{} without assuming that the presheaves are locally representable;
in that case we talk about preservation of virtual context extensions.
% Indeed context extensions are limits, and although they may fail to exist in some base category $\CC$, they will always exist in the presheaf category $\CPsh^{\CC}$.
% A functor $F : \CC \to \CD$ preserves virtual limits if $F_{!} : \CPsh^{\CC} \to \CPsh^{\CD}$ preserves the corresponding limits.

\begin{definition}[Internally to $\CPsh^{\CC}$]\label{def:preserves_ext}
  Let $A^{\CC} : \SPsh^{\CC}$ and $A^{\CD} : \mmod{F^{\ast}} \to \SPsh^{\CD}$ be presheaves over $\CC$ and $\CD$, and $F^{A} : \forall (a : A^{\CC}) \mmod{F^{\ast}} \to A^{\CD}\ \mmod{F^{\ast}}$ be an action of $F$ on the elements of $A^{\CC}$.
  We say that $F^{A}$ \defemph{preserves virtual context extensions} if for every dependent presheaf $P : \forall \mmod{F^{\ast}} (a : A^{\CD}\ \mmod{F^{\ast}}) \to \SPsh^{\CD}$, the canonical comparison map
  \begin{alignat*}{3}
    & \tau && :{ } && (\forall \mmod{F^{\ast}} (a : A^{\CD}\ \mmod{F^{\ast}}) \to P\ \mmod{F^{\ast}}\ a) \to (\forall (a : A^{\CC}) \mmod{F^{\ast}} \to P\ \mmod{F^{\ast}}\ (F^{A}\ a\ \ \mmod{F^{\ast}})) \\
    & \tau(p) && \triangleq{ } && \lambda a \mmod{F^{\ast}} \mapsto p\ \mmod{F^{\ast}}\ (F^{A}\ a\ \mmod{F^{\ast}})
  \end{alignat*}
  is an isomorphism.
  % Note that this map could alternatively be defined as $\tau\ p \triangleq \lambda\ a \mapsto p \circledast F^{A}\ a$.
  In other words, $F^{A}$ preserves virtual context extensions when the modality $(\mmod{F^{\ast}} \to -)$ commutes with quantification over $A^{\CC}$ and $A^{\CD}$.

  This provides a notation to define an element $p$ of $(\forall \mmod{F^{\ast}}(a : A^{\CD}\ \mmod{F^{\ast}}) \to P\ \mmod{F^{\ast}}\ a)$ using pattern matching: we write
  \[ p\ \mmod{F^{\ast}}\ (F^{A}\ a\ \mmod{F^{\ast}}) \triangleq q\ a\ \mmod{F^{\ast}} \]
  to define $p$ as the unique solution of that equation ($p = \tau^{-1}(\lambda a \mmod{F^{\ast}} \mapsto q\ a\ \mmod{F^{\ast}})$).
  \lipicsEnd
\end{definition}

In \cref{sec:preserv} we show that the internal description of preservation of context extensions coincides with the external notion of preservation up to isomorphism.

\subsection{Morphisms of models}
\label{subsec:morphisms}

Let $F : \CC \to \CD$ be a morphism of models of $\Th_{\Pi,\BoolTy}$.
We now show that its structure can fully be described in the internal language of $\CPsh^{\CC}$.

Its actions on types and terms can equivalently be given by the following global elements.
\begin{alignat*}{3}
  & F^{\Ty} && :{ } && (A : \Ty^{\CC}) \to (\mmod{F^{\ast}} \to \Ty^{\CD}) \\
  & F^{\Tm} && :{ } && \forall A\ (a : \Tm^{\CC}) \to (\mmod{F^{\ast}} \to \Tm^{\CD}\ (F^{\Ty}\ A\ \mmod{F^{\ast}}))
\end{alignat*}
The preservation of context extensions by $F$ is equivalent to the fact that $F^{\Tm}$ preserves virtual context extensions in the sense of \cref{def:preserves_ext}.
We can use that fact to obtain the following actions on derived sorts.
\begin{alignat*}{3}
  & F^{[X]\Ty} && :{ } && (A : X^{\CC} \to \Ty^{\CC}) \to (\forall \mmod{F^{\ast}}\ (x : X^{\CD}) \to \Ty^{\CD}) \\
  & F^{[X]\Tm} && :{ } && \forall A\ (a : (x : X^{\CC}) \to \Tm^{\CC}\ (A\ x)) \to (\forall \mmod{F^{\ast}}\ (x : X^{\CD}) \to \Tm^{\CD}(F^{[X]\Ty}\ A\ \mmod{F^{\ast}}(x)))
\end{alignat*}
They are defined as follows, using the pattern matching notation of \cref{def:preserves_ext}.
\begin{alignat*}{3}
  & F^{[X]\Ty}\ A\ \mmod{F^{\ast}}\ (F^{X}\ x\ \mmod{F^{\ast}}) && \triangleq{ } && F^{\Ty}\ (A\ x)\ \mmod{F^{\ast}} \\
  & F^{[X]\Tm}\ a\ \mmod{F^{\ast}}\ (F^{X}\ x\ \mmod{F^{\ast}}) && \triangleq{ } && F^{\Tm}\ (a\ x)\ \mmod{F^{\ast}}
\end{alignat*}
Finally, the preservation of the operations can simply be described by the following equations.
\begin{alignat*}{1}
  & F^{\Ty}\ (\Pi^{\CC}\ A\ B)\ \mmod{F^{\ast}} = \Pi^{\CD}\ (F^{\Ty}\ A\ \mmod{F^{\ast}})\ (F^{[\Tm]\Ty}\ B\ \mmod{F^{\ast}}) \\
  & F^{\Ty}\ (\app^{\CC}\ f\ a)\ \mmod{F^{\ast}} = \app^{\CD}\ (F^{\Tm}\ f\ \mmod{F^{\ast}})\ (F^{\Tm}\ a\ \mmod{F^{\ast}}) \\
  & F^{\Ty}\ \BoolTy^{\CC}\ \ \mmod{F^{\ast}} = \BoolTy^{\CD} \\
  & F^{\Ty}\ \true^{\CC}\ \ \mmod{F^{\ast}} = \true^{\CD} \\
  & F^{\Ty}\ \false^{\CC}\ \ \mmod{F^{\ast}} = \false^{\CD} \\
  & F^{\Ty}\ (\elimBool^{\CC}\ t\ f\ b)\ \mmod{F^{\ast}} = \elimBool^{\CD}\ (F^{[\Tm]\Ty}\ P\ \mmod{F^{\ast}})\ (F^{\Tm}\ t\ \mmod{F^{\ast}})\ (F^{\Tm}\ f\ \mmod{F^{\ast}})\ (F^{\Tm}\ b\ \mmod{F^{\ast}})
\end{alignat*}
We can then derive analogous equations for $F^{[X]\Ty}$ and $F^{[X]\Tm}$.
For instance,
\begin{alignat*}{1}
  & F^{[X]\Ty}\ (\lambda x \mapsto \Pi^{\CC}\ (A\ x)\ (B\ x))\ \mmod{F^{\ast}}\ x \\
  & \quad = \Pi^{\CD}\ (F^{[X]\Ty}\ A\ \mmod{F^{\ast}}\ x)\ (\lambda a \mapsto F^{[X,\Tm]\Ty}\ B\ \mmod{F^{\ast}}\ (x,a)).
\end{alignat*}
Indeed, by \cref{def:preserves_ext}, it suffices to show that equation when $x = F^{X}\ x'\ \mmod{F^{\ast}}$.
It then follows from the base equation for $F^{\Ty}\ (\Pi^{\CC}\ (A\ x')\ (B\ x'))$.

We can also derive strengthening equations.
For example, when $A$ does not depend on $X$, we have $F^{[X]\Ty}\ (\lambda x \mapsto A)\ \mmod{F^{\ast}} = \lambda x \mapsto F^{\Ty}\ A\ \mmod{F^{\ast}}$.

\begin{remark}
  The notion of morphism of models unfolds externally to the verbose notion of algebra morphism for the QIIT signature of \cref{rmk:QIIT}, except that we do not require context extension to be preserved strictly.
  A standard argument shows that initial algebras for the QIIT are biinitial in our sense.
  A similar remark holds for the notion of displayed model (and their sections) that will be defined in \cref{sec:disp_models_wo_exts}.
\end{remark}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
