\section{Internal language of presheaf categories and models of type theory}\label{sec:int_lang_psh}

\begin{itemize}
  \item We work in a constructive metatheory, with a cumulative hierarchy $(\SSet_{i})$ of universes.
  \item If $\CC$ is a small category, we write $\abs{\CC}$ for its set of objects and $\CC(x \to y)$ for the set of morphisms from $x$ to $y$.
        We may write $(x : \CC)$ (or $(x : \CC^{\op})$) instead of $(x : \abs{\CC})$ to indicate that the dependence on $x$ is covariant (or contravariant).

        We write $(f \cdot g)$ or $(g\circ f)$ for the composition of $f : \CC(x \to y)$ and $g : \CC(y \to z)$.
  \item We rely on the internal language of presheaf categories. Given a small category $\CC$, the presheaf category $\CPsh^{\CC}$ is a model of extensional type theory, with a cumulative hierarchy of universes $\SPsh^{\CC}_{0} \subset \SPsh^{\CC}_{1} \subset \cdots \subset \SPsh^{\CC}_{i} \subset \cdots$, dependent functions, dependent sums, quotient inductive-inductive types, extensional equality types, \etc.
        For each of our definitions, propositions, theorems, \etc, we specify whether it should be interpreted externally or internally to some presheaf category.
  \item The Yoneda embedding is written $\yo : \CC \to \CPsh^{\CC}$.
        We denote the restriction of an element $x : \abs{X}_{\Gamma}$ of a presheaf $X$ along a morphism $\rho : \CC(\Delta \to \Gamma)$ by $x[\rho]_X : \abs{X}_\Delta$.
\end{itemize}

\subsection{Locally representable presheaves}

The notion of locally representable presheaf is the semantic counterpart of the notion of context extension.

\begin{definition} \label{locally-representable}
  Let $X$ be a presheaf over a category $\CC$ and $Y$ be a dependent presheaf over $X$.
  We say that $Y$ is \defemph{locally representable} if, for every $\Gamma : \abs{\CC}$ and $x : \abs{X}_{\Gamma}$, the presheaf
  \begin{alignat*}{3}
    & Y_{\mid x} && :{ } && \forall (\Delta : \CC^{\op}) (\rho : \CC(\Delta \to \Gamma)) \to \CSet \\
    & \abs{Y_{\mid x}}\ \rho && \triangleq{ } && \abs{Y}_{\Delta}\ (x[\rho])
  \end{alignat*}
  over the slice category $(\CC / \Gamma)$ is representable.

  In that case, we have, for every $\Gamma$ and $x$, an \defemph{extended context} $(\Gamma \rhd Y_{\mid x})$, a \defemph{projection map} $\bm{p}^{Y}_{x} : (\Gamma \rhd Y_{\mid x}) \to \Gamma$ and a \defemph{generic element} $\bm{q}^{Y}_{x} : \abs{Y_{\mid x}}\ \bm{p}_{x}^{Y}$ such that for every $\sigma : \Delta \to \Gamma$ and $y : \abs{Y_{\mid x}}\ \sigma$, there is a unique \defemph{extended morphism} $\angles{\sigma,y} : \Delta \to (\Gamma \rhd Y_{\mid x})$ such that $\angles{\sigma,y} \cdot \bm{p}^{Y}_{x} = \sigma$ and $\bm{q}^{Y}_{x}[\angles{\sigma,y}] = y$.
  \lipicsEnd
\end{definition}

Up to the correspondence between dependent presheaves and their total maps, locally representable dependent presheaves are also known as \emph{representable natural transformations} \cite{awodey_2018}.
We read this definition in a structured manner, with a local representability structure consisting of a choice of representing objects in the above definition.
The notion of local representability is \emph{local}: the restriction map from local representability structures for a dependent presheaf $Y$ over $X$ to coherent families of local representability structures of $Y_{\mid x}$ over $\yo_{\Gamma}$ for $x : \abs{X}_{\Gamma}$ is invertible.%
\footnote{Locality also holds if we consider local representability as a property.}

Assume that $\CC$ is an $i$-small category.
Internally to $\CPsh^{\CC}$ there is then, for every universe level $j$, a family $\isRep : \SPsh^{\CC}_{j} \to \SPsh^{\CC}_{\max(i, j)}$ of local representability structures over $j$-small presheaf families.
Due to the above locality property, we have for a dependent presheaf $Y$ over $X$ that elements of $X \vdash \isRep(Y)$ correspond to witnesses that $Y$ is locally representable over $X$.
This leads to universes $\SRepPsh^{\CC}_{j} \triangleq (A : \SPsh^{\CC}_{j}) \times \isRep\ A$ of $j$-small locally representable presheaf families.
As an internal category, it is equivalent to the $j$-small one that at $\Gamma : \abs{\CC}$ consists of an element of the slice of $\CC$ over $\Gamma$ together with a choice of base changes along any map $\CC(\Delta \to \Gamma)$. % can remove if too long

An alternative semantic for the presheaf $(y : Y) \to Z\ y$ of dependent natural transformations from $Y$ to $Z$ can be given when $Y$ is locally representable over $X : \CPsh^{\CC}$.
We could define $\abs{(y : Y) \to Z\ y}_{\Gamma}\ x \triangleq \abs{Z}_{\Gamma \rhd Y_{\mid x}}\ (x[\bm{p}^{Y}_{x}], \bm{q}^{Y}_{x})$.
This definition satisfies the universal property of the presheaf of dependent natural transformations from $Y$ to $Z$, and is therefore isomorphic to its usual definition.
The alternative definition admits a generalized algebraic presentation, which is important to justify the existence of initial models.

\subsection{Internal definition of models}

Our main running example is the theory $\Th_{\Pi,\BoolTy}$ of a family equipped with $\Pi$-types and a boolean type.
An internal model of $\Th_{\Pi,\BoolTy}$ in a presheaf category $\CPsh^{\CC}$ consists of the following elements.
\begin{alignat*}{3}
  & \Ty && :{ } && \SPsh^{\CC} \\
  & \Tm && :{ } && \Ty \to \SRepPsh^{\CC} \\
  & \Pi && :{ } && \forall (A : \Ty) (B : \Tm\ A \to \Ty) \to \Ty \\
  & \app && :{ } && \forall A\ B \to \Tm\ (\Pi\ A\ B) \simeq ((a : \Tm\ A) \to \Tm\ (B\ a))
\end{alignat*}
\begin{alignat*}{3}
  & \BoolTy && :{ } && \Ty \\
  & \true,\false && :{ } && \Tm\ \BoolTy \\
  & \elimBool && :{ } && \forall (P : \Tm\ \BoolTy \to \Ty)\ (t : \Tm\ (P\ \true))\ (f : \Tm\ (P\ \false))\ (b : \Tm\ \BoolTy) \to \Tm\ (P\ b) \\
  & - && :{ } && \elimBool\ P\ t\ f\ \true = t \\
  & - && :{ } && \elimBool\ P\ t\ f\ \false = f
\end{alignat*}
The inverse of $\app$ is written $\lam : \forall A\ B \to ((a : \Tm\ A) \to \Tm\ (B\ a)) \to \Tm\ (\Pi\ A\ B)$.

A model of $\Th_{\Pi,\BoolTy}$ is a category $\CC$ equipped with a terminal object and with a global internal model of $\Th_{\Pi,\BoolTy}$ in $\CPsh^{\CC}$.

\begin{remark}\label{rmk:QIIT}
  If we unfold the above internal definitions in presheaves, we see that a model of $\Th_{\Pi,\BoolTy}$ is the same externally as an algebra for the signature of a quotient inductive-inductive type (QIIT)~\cite{QIITs} describing $\Th_{\Pi,\BoolTy}$.
  That QIIT is significantly more verbose because it has sorts of contexts and substitutions and, for every component of the model, separately states the action at each context and coherent action of or coherence under substitution.
  The notion of morphism of models we will define in \cref{subsec:morphisms} unfolds externally to the verbose notion of algebra morphism for this QIIT, except that we do not require context extension to be preserved strictly.
  The same remark holds for the notion of displayed model to be defined in \cref{sec:disp_models_wo_exts}.
\end{remark}

We have a $(2,1)$-category $\CMod_{\Th_{\Pi,\BoolTy}}$ of models.
The morphisms are functors equipped with actions on types and terms that preserve the terminal object and the context extensions up to isomorphisms and the operations $\Pi$, $\app$, $\BoolTy$, $\true$, $\false$ and $\elimBool$ strictly.
The $2$-cells are the natural isomorphisms between the underlying functors.

We have just given an internal definition of the objects of $\CMod_{\Th_{\Pi,\BoolTy}}$ in the language of presheaf categories; we will give internal definitions of the other components using dependent right adjoints.

\subsection{Sorts and derived sorts}

A base sort of a CwF $\CC$ is a (code for a) presheaf (in $\CPsh^{\CC}$) of the form $\Ty$ or $\Tm(-)$.
The derived sorts are obtained by closing the base sorts under dependent products with arities in $\Tm(-)$.
A derived sort is either a base sort, or a presheaf of the form $(a : \Tm(-)) \to X(a)$ where $X(a)$ is a derived sort.
A derived sort can be written in the form $[X]Y$ where $X$ is a telescope of types and $Y$ is a base sort that depends on $X$.

The type of an argument of a type-theoretic operation or equation is always a derived sort.
We often omit dependencies when writing derived sorts; \eg{} we write $[\Tm]\Ty$ for the derived sort of the second argument of $\Pi$.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
