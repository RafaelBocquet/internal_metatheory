
\section{The dependent right adjoints \texorpdfstring{$F^{\ast}$}{F\textasciicircum *} and \texorpdfstring{$F_{\ast}$}{F\_{}*}}\label{sec:dra_constructions}

In this section we give explicit definitions of the adjunctions $F_{!} \dashv F^{\ast}$ and $F^{\ast} \dashv F_{\ast}$ and their dependent versions $F_{!} \dashv \modcolor{F^{\ast}}$ and $F^{\ast} \dashv \modcolor{F_{\ast}}$, given a functor $F : \CC \to \CD$.
These definitions are standard category theory, we only record them for the benefit of the reader.

The precomposition functor:
\begin{alignat*}{3}
  & F^{\ast} && :{ } && \CPsh^{\CD} \to \CPsh^{\CC} \\
  & \abs{F^{\ast}\ X'}_\Gamma && \triangleq{ } && \abs{X'}_{F\ \Gamma} \\
  & x[\rho]_{F^{\ast}\ X'} && \triangleq{ } && x[F\ \rho]_{X'} \\
  & \abs{F^{\ast}\ f'}_\Gamma\ x && \triangleq{ } && \abs{f'}_{F\ \Gamma}\ x
\end{alignat*}
Its left adjoint:
\begin{alignat*}{3}
  & F_{!} && :{ } && \CPsh^{\CC} \to \CPsh^{\CD} \\
  & \abs{F_{!}\ X}_{\Gamma'} && \triangleq{ } &&
  \big((\Gamma:\abs{\CC})\times\CD(\Gamma'\to F\ \Gamma)\times\abs{X}_{\Gamma}\big) /{\sim} \text{ where } (\Gamma,\delta',x[\rho]_X) \sim (\Delta,F\ \rho\circ\delta',x) \\
  & (\Gamma,\delta',x)[\rho']_{F_{!}\ X} && \triangleq{ } && (\Gamma,\delta'\circ\rho',x) \\
%  & \text{ where } (\Gamma,\delta',x[\rho]_X)[\rho']_{F_{!}\ X} = (\Gamma,\delta'\circ\rho',x[\rho]_X) = (\Delta,F\ \rho\circ\delta'\circ\rho',x) = (\Delta,F\ \rho\circ\delta',x)[\rho']_{F_{!}\ X} \\
  & \abs{F_{!}\ f}_{\Gamma'}\ (\Gamma,\delta',x) && \triangleq{ } && (\Gamma,\delta',\abs{f}_\Gamma\ x)
%  & \text{ where } \hspace{2em} \abs{F_{!}\ f}_{\Gamma'}\ (\Gamma,\delta',x[\rho]_X) = (\Gamma,\delta',\abs{f}_\Gamma\ (x[\rho]_X)) =  (\Gamma,\delta',(\abs{f}_\Gamma\ x)[\rho]_Y) = (\Delta,F\ \rho\circ\delta',\abs{f}_\Gamma\ x) =  \abs{F_{!}\ f}_{\Gamma'}\ (\Delta,F\ \rho\circ\delta',x)
\end{alignat*}
The unit of the adjunction $F_{!} \dashv F^{\ast}$ is given by
\begin{alignat*}{3}
  & \eta_X && :{ } && X \to (F^{\ast}\ (F_{!}\ X)) \\
  & \abs{\eta_X}_\Gamma\ x && \triangleq{ } && (\Gamma,\id_{F\ \Gamma},x)
\end{alignat*}
while the hom-set definition of the adjunction is given by an isomorphism
\[
  \phi : (F_{!}\ X\to X') \cong (X\to F^{\ast}\ X') : \phi^{-1}
\]
natural in $X$ and $X'$, where $\phi\ f' \triangleq
F^{\ast}\ f'\circ\eta_X$ \ie $\abs{\phi\ f'}_\Gamma\ x =
\abs{f'}_{F\ \Gamma}\ (\abs{\eta_X}_\Gamma\ x)$ and
$\abs{\phi^{-1}\ f}_{\Gamma'}\ (\Gamma,\delta',x) \triangleq
(\abs{f}_\Gamma\ x)[\delta']_{X}$. The dependent right adjoint of $F_{!}$:
\begin{alignat*}{3}
  & \modcolor{F^{\ast}} && :{ } && \CDepPsh^{\CD}\ (F_{!}\ X) \to \CDepPsh^{\CC}\ X \\
  & \abs{\modcolor{F^{\ast}}\ A'}_\Gamma\ x && \triangleq{ } && \abs{A'}_{F\ \Gamma}\ (\abs{\eta_X}_\Gamma\ x) \\
  & a'[\rho]_{\modcolor{F^{\ast}}\ A'} && \triangleq{ } && a'[F\ \rho]_{A'}
\end{alignat*}
We have $\modcolor{F^{\ast}}\ A' \circ f = \modcolor{F^{\ast}}\ (A'\circ F_{!}\ f)$. The dependent adjunction $F_{!} \dashv \modcolor{F^{\ast}}$ is an isomorphism
\[
\psi : \CPsh^{\CD}\big((x':F_{!}\ X) \to A'(x')\big) \cong \CPsh^{\CC}\big((x:X)\to (\modcolor{F^{\ast}}\ A')(x)\big) : \psi^{-1}
\]
natural in $X$, where $\abs{\psi\ f'}_\Gamma\ x \triangleq \abs{f'}_{F\ \Gamma}\ (\abs{\eta_X}_\Gamma\ x)$ and $\abs{\psi^{-1}\ f}_{\Gamma'}\ (\Gamma,\delta',x) \triangleq (\abs{f}_\Gamma x)[\delta']_{A'}$.
% Internally to $\CPsh^{\CC}$, we write $\lock{}{F^{\ast}}\to A'$ for $\modcolor{F^{\ast}}\ A'$, $\lambda\lock{}{F^{\ast}}\mapsto f'$ for $\psi\ f'$ and $f\unlock{}{F^{\ast}}$ for $\psi^{-1}\ f$.

The right adjoint of $F^{\ast}$:
\begin{alignat*}{3}
  & F_{\ast} && :{ } && \CPsh^{\CC} \to \CPsh^{\CD} \\
  & \abs{F_{\ast}\ X}_{\Gamma'} && \triangleq{ } && \big\{ \alpha:(\Gamma:\abs{\CC})(\delta':\CD(F\ \Gamma\to\Gamma'))\to\abs{X}_{\Gamma} \mid \alpha\ \Gamma\ (\delta'\circ F\ \sigma) = (\alpha\ \Delta\ \delta')[\sigma]_X \big\} \\
  & \alpha[\rho']_{F_{\ast}\ X} && \triangleq{ } && \lambda \Gamma\ \delta' \mapsto \alpha\ \Gamma\ (\rho'\circ\delta') \\
  & \abs{F_{\ast}\ f}_{\Gamma'}\ \alpha && \triangleq{ } && \lambda \Gamma\ \delta' \mapsto \abs{f}_\Gamma\ (\alpha\ \Gamma\ \delta')
\end{alignat*}
The adjunction is an isomorphism $\phi : (F^{\ast}\ X'\to X) \cong
(X'\to F_{\ast}\ X) : \phi^{-1}$ natural in $X$ and $X'$ where $\abs{\phi\ f}_{\Gamma'}\ x' \triangleq \lambda \Gamma\ \delta' \mapsto \abs{f}_\Gamma\ (x'[\delta']_{X'})$ and
$\abs{\phi^{-1}\ f'}_{\Gamma}\ x' \triangleq \abs{f'}_{F\ \Gamma}\ x'\ \Gamma\ \id_{F\ \Gamma}$.
The dependent right adjoint of $F^{\ast}$:
\begin{alignat*}{5}
  & \modcolor{F_{\ast}} && :{ } && \CDepPsh^{\CC}\ (F^{\ast}\ X') \to \CDepPsh^{\CD}\ X' \\
  & \abs{\modcolor{F_{\ast}}\ A}_{\Gamma'}\ x' && \triangleq{ } && \big\{ \alpha:(\Gamma:\abs{\CC})(\delta':\CD(F\ \Gamma\to\Gamma'))\to\abs{A}_{\Gamma}\ (x'[\delta']_{X'}) \mid \\
  & && && \hphantom{\big\{{}} \alpha\ \Gamma\ (\delta'\circ F\ \sigma) = (\alpha\ \Delta\ \delta')[\sigma]_A \big\} \\
  & \alpha [\rho']_{\modcolor{F_{\ast}}\ A} && \triangleq{ } && \lambda \Gamma\ \delta' \mapsto \alpha\ \Gamma\ (\rho'\circ\delta')
\end{alignat*}
We have $\modcolor{F_{\ast}}\ A \circ f' = \modcolor{F^{\ast}}\ (A\circ F_{!}\ f')$. The dependent adjunction $F^{\ast} \dashv \modcolor{F_{\ast}}$ is an isomorphism
\[
\psi : \CPsh^{\CC}\big((x:F^{\ast}\ X') \to A(x)\big) \cong \CPsh^{\CD}\big((x':X')\to (\modcolor{F_{\ast}}\ A)(x')\big) : \psi^{-1}
\]
natural in X' where $\abs{\psi\ f}_{\Gamma'}\ x' \triangleq \lambda\Gamma\ \delta'\mapsto \abs{f}_\Gamma\ (x'[\delta']_{X'})$ and $\abs{\psi^{-1}\ f'}_\Gamma\ x' \triangleq \abs{f'}_{F\ \Gamma}\ x'\ \Gamma\ \id_{F\ \Gamma}$.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
