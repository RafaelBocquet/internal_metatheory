\section{Multimodal Type Theory}\label{sec:mtt}

The proofs and constructions performed in the appendix involve more than two presheaf categories and more than a single dependent right adjoint.
We rely on Multimodal Type Theory~\cite{MTT} to provide a single language that embeds the internal languages of all of those presheaf categories and the dependent right adjoints between them.

Our variant of Multimodal Type Theory differs from the one presented in~\cite{MTT} in a couple of ways.
We keep the same syntax for dependent right adjoints as in \cref{sec:dras}; whereas~\cite{MTT} uses weak dependent right adjoints instead, which come with a positive elimination rule instead of the operation $(-\ \mmod{\mu})$.
So as to remove some of the ambiguities of the informal syntax and improve readability in the presence of multiple modalities, we annotate locks with \emph{tick variables}.
The extension of the syntax of Multimodal Type Theory by ticks was used by~\cite{Transpension} for the same purpose.
Ticks were originally introduced in~\cite{ClocksAreTicking}.


\subsection{Multiple modalities}

Multiple modalities are given semantically by multiple dependent right adjoints.
Given a functor $F : \CC \to \CD$, we already have two dependent right adjoints $\modcolor{F^{\ast}}$ and $\modcolor{F_{\ast}}$, which give modalities $(\mmod{F^{\ast}} \to -)$ and $(\mmod{F_{\ast}} \to -)$.
Dependent right adjoints can be composed, and we also have modalities $(\mmod{F_{\ast}F^{\ast}} \to -)$, $(\mmod{F^{\ast}F_{\ast}} \to -)$, \etc, where $(\mmod{F_{\ast}F^{\ast}} \to -) = (\mmod{F_{\ast}} \mmod{F^{\ast}} \to -)$.

\subsubsection{Ticks}

In presence of multiple modalities, or of non-trivial relations between the modalities, the notation $(-\ \mmod{\mu})$ becomes ambiguous.
Suppose for instance that $\modcolor{\mu}$ is a idempotent dependent right adjoint ($\modcolor{\mu \mu} = \modcolor{\mu}$).
Then for any context $\Gamma$, we have $\Gamma, \mmod{\mu}, \mmod{\mu} = \Gamma, \mmod{\mu}$.
If we write $(a\ \mmod{\mu})$ in the ambient context $(\Gamma, \mmod{\mu})$, it is unclear whether the subterm $a$ should live in the context $\Gamma$ or $\Gamma, \mmod{\mu}$.

To avoid this kind of ambiguity, we will annotate locks with \emph{ticks}.
In the above example, we would have $\Gamma, \emod{\mfm}{\mu}, \emod{\mfn}{\mu} = \Gamma, \emod{\mfm\mfn}{\mu}$; and we would write either $(a\ \smod{\mfn})$ if $a$ lives over $\Gamma,\emod{\mfm}{\mu}$ or $(a\ \smod{\mfm \mfn})$ if $a$ lives over $\Gamma$.

We use $\tickcolor{\mfm}$, $\tickcolor{\mfn}$, $\tickcolor{\mfo}$, \etc for tick variables.
The tick variables refer to the locks of the ambient context.
A tick is a formal composition of tick variables, corresponding to the composition of some adjacent locks in the ambient context.
We write $\tickcolor{\bullet}$ for the empty tick, corresponding to the empty composition.
We write $\tickcolor{\overline{\mfm}}$, $\tickcolor{\overline{\mfn}}$, $\tickcolor{\overline{\mfo}}$, \etc to refer to an arbitrary tick.

If $\Gamma$ is a context, then the subterms of $(\emod{\mfm}{\mu} \to -)$ and $(\lambda\ \emod{\mfm}{\mu} \mapsto -)$ live over the context $\Gamma, \emod{\mfm}{\mu}$.

The operation $(-\ \smod{\overline{\mfm}})$ now unbinds the last tick variable of the context; or more generally some suffix of the tick variables.
The ordinary variables that occur after these tick variables are implicitly dropped from the current context.

We omit ticks when no ambiguity can arise.
In fact, we don't need to use ticks outside of this section.

\subsubsection{Morphisms between modalities}

Finally, we have morphisms between modalities.
If $\modcolor{\mu}$ and $\modcolor{\nu}$ are two parallel dependent right adjoints, whose left adjoints are respectively $L_{\mu}$ and $L_{\nu}$, a morphism $\natcolor{\alpha} : \modcolor{\mu} \Ra \modcolor{\nu}$ is a natural transformation $\alpha : L_{\mu} \Ra L_{\nu}$.
For example, given $F : \CC \to \CD$, we have a counit $\natcolor{\varepsilon^{F}} : \modcolor{F_{\ast}} \modcolor{F^{\ast}} \Ra \modcolor{1}$, and a unit $\natcolor{\eta^{F}} : \modcolor{1} \Ra \modcolor{F^{\ast}} \modcolor{F_{\ast}} $, induced by the adjunction $(F_{!} \dashv F^{\ast})$.

Given $\natcolor{\alpha} : \modcolor{\mu} \Ra \modcolor{\nu}$, we obtain a coercion operation $-\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}}$ that sends types and terms from the context $\Gamma, \emod{\overline{\mfn}}{\nu}$ to the context $\Gamma, \emod{\overline{\mfm}}{\mu}$.
Semantically, this operation is the presheaf restriction operation of types and terms along the morphism $\abs{\alpha}_{\Gamma} : (\Gamma, \emod{\overline{\mfm}}{\mu}) \to (\Gamma, \emod{\overline{\mfn}}{\nu})$.

This induces a map
\begin{alignat*}{3}
  & \mathsf{coe}_{\alpha} && :{ } && \forall (A : \emod{\mfn}{\nu} \to \SPsh) \to (\emod{\mfn}{\nu} \to A\ \smod{\mfn}) \to (\emod{\mfm}{\mu} \to (A\ \smod{\mfn})\ekey{\alpha}{\mfm}{\mfn}) \\
  & \mathsf{coe}_{\alpha}\ a && \triangleq{ } && \lambda\ \emod{\mfm}{\mu} \mapsto (a\ \smod{\mfn})\ekey{\alpha}{\mfm}{\mfn}
\end{alignat*}

For another example, consider composable functors $F : \CC \to \CD$ and $G : \CD \to \CE$.
We have a natural isomorphism $\alpha : (F G)_{!} \simeq F_{!} G_{!}$.
This induces isomorphisms $(\emod{\mfm}{{(FG)}^{\ast}} \to A) \simeq (\emod{\mff\mfg}{F^{\ast}G^{\ast}} \to A)$, whose components are
\[ \lambda a\ \emod{\mff\mfg}{F^{\ast}G^{\ast}} \mapsto (a\ \smod{\mfm})\ekey{\alpha}{\mff \mfg}{\mfm} \]
and
\[ \lambda a\ \emod{\mfm}{(FG)^{\ast}} \mapsto (a\ \smod{\mff\mfg})\ekey{\alpha\inv}{\mfm}{\mff \mfg}. \]

We omit the natural transformation when it can be inferred.
For instance we could have written $\skey{\mff\mfg}{\mfm}$ and $\skey{\mfm}{\mff\mfg}$ above.

More generally, the operation $\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}}$ can be applied to any type or term over a context of the form $(\Gamma, \emod{\overline{\mfn}}{\nu}, \Delta)$ to send it to the context $(\Gamma, \emod{\overline{\mfm}}{\mu}, \Delta\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}})$, where $\Delta$ is an extension of the context $(\Gamma, \emod{\overline{\mfn}}{\nu})$ by variable bindings and locks, and $\Delta\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}}$ applies the operation $\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}}$ to every type in $\Delta$.
In that case it is interpreted semantically by restriction along the weakening $(\Gamma, \emod{\overline{\mfm}}{\mu}, \Delta\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}}) \to (\Gamma, \emod{\overline{\mfm}}{\mu}, \Delta)$ of $\abs{\alpha}_{\Gamma} : (\Gamma, \emod{\overline{\mfm}}{\mu}) \to (\Gamma, \emod{\overline{\mfn}}{\nu})$.

The operation $\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}}$ commutes with all natural type-theoretic operations.
For example, $(A \times B)\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}} = (A\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}} \times B\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}})$.

It commutes with binders:
\[ ((a : A) \to B\ a)\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}} = (a : A\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}}) \to B\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}}\ a. \]
Note that $\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}}$ is not applied to the bound variable $a$, as it is already applied to the type $A$ of $a$.

It also commutes with $(\mmod{\mu} \to -)$ and $(\lambda\ \mmod{\mu} \mapsto -)$ for a dependent right adjoint $\mu$:
\[ (\emod{\mfo}{\mu} \to A)\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}} = (\emod{\mfo}{\mu} \to A\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}}) \]
\[ (\lambda\ \emod{\mfo}{\mu} \mapsto a)\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}} = (\lambda\ \emod{\mfo}{\mu} \mapsto a\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}}) \]

It commutes with $(-\ \smod{\mfo})$ when $\tickcolor{\mfo}$ is a tick variable that is bound in $\Delta$.

The operation $\ekey{\alpha}{\overline{\mfm}}{\overline{\mfn}\mfq}$ (where $\tickcolor{\overline{\mfn}\mfq}$ is a non-empty composition of tick variables ending in $\tickcolor{\mfq}$) can only get stuck on $(-\ \smod{\mfq})$ (or more generally on $(-\ \smod{\overline{\mfo}\mfq})$).
The operation $\ekey{\alpha}{\overline{\mfm}}{\bullet}$ (where $\tickcolor{\bullet}$ is the empty composition of ticks) can only be stuck on a variable.

Finally, these operations satisfy some $2$-naturality conditions.
Given two vertically composable morphisms $\natcolor{\alpha} : \modcolor{\mu} \Ra \modcolor{\nu}$ and $\natcolor{\beta} : \modcolor{\nu} \Ra \modcolor{\xi}$, we have
\[ (-)\ekey{\beta}{\mfn}{\mfx}\ekey{\alpha}{\mfm}{\mfn} = (-)\ekey{\alpha\beta}{\mfm}{\mfx}. \]
Given $\natcolor{\alpha} : \modcolor{\mu} \Ra \modcolor{\nu}$ and a dependent right adjoint $\modcolor{\xi}$ such that the whiskering $\natcolor{\alpha}\modcolor{\xi}$ can be formed, we have
\[ (-)\ekey{\alpha\modcolor{\xi}}{\mfm\mfx}{\mfn\mfx} = (-)\ekey{\alpha}{\mfm}{\mfn}. \]
Similarly, when we can form the whiskering $\modcolor{\xi}\natcolor{\alpha}$, we have
\[ (-)\ekey{\modcolor{\xi}\alpha}{\mfx\mfm}{\mfx\mfn} = (-)\ekey{\alpha}{\mfm}{\mfn}. \]


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
