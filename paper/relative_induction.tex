\section{Relative induction principles}\label{sec:disp_models_wo_exts}

In this section we state our relative induction principles using the notion of displayed model without context extensions.
The full proofs of these relative induction principles are given in the appendix.

We fix a base model $\CS$ of $\Th_{\Pi,\BoolTy}$ and a functor $F : \CC \to \CS$.

\begin{definition}
  A \defemph{displayed model without context extensions} over $F : \CC \to \CS$ consists of the following components, specified internally to $\CPsh^{\CC}$:
  \begin{itemize}
    \item Presheaves of displayed types and terms.
          \begin{alignat*}{3}
            & \Ty^{\bullet} && :{ } && (A : \mmod{F^{\ast}} \to \Ty^{\CS}) \to \SPsh^{\CC} \\
            & \Tm^{\bullet} && :{ } && \forall A\ (A^{\bullet} : \Ty^{\bullet}\ A) (a : \mmod{F^{\ast}} \to \Tm^{\CS}\ (A\ \mmod{F^{\ast}})) \to \SPsh^{\CC}
          \end{alignat*}

          They correspond to the \emph{motives} of an induction principle.

    \item Displayed variants of the type-theoretic operations of $\Th_{\Pi,\BoolTy}$.
          They are the \emph{methods} of the induction principle.
          \begin{alignat*}{3}
            & \Pi^{\bullet} && :{ } && \forall A\ B\ (A^{\bullet} : \Ty^{\bullet}\ A) (B^{\bullet} : \{a\} (a^{\bullet} : \Ty^{\bullet}\ A^{\bullet}\ a) \to \Ty^{\bullet}\ (B \circledast a)) \\
            &&&&& \to \Ty^{\bullet}\ (\Pi^{\CS} \circleddollar A \circledast B) \\
            & \app^{\bullet} && :{ } && \forall A\ B\ f\ a\ (A^{\bullet} : \Ty^{\bullet}\ A) (B^{\bullet} : \{a\} (a^{\bullet} : \Ty^{\bullet}\ A^{\bullet}\ a) \to \Ty^{\bullet}\ (B \circledast a)) \\
            &&&&& \to (\Tm^{\bullet}\ (\Pi^{\bullet}\ A^{\bullet}\ B^{\bullet})\ f) \simeq ((a^{\bullet} : \Ty^{\bullet}\ A^{\bullet}\ a) \to \Tm^{\bullet}\ (B^{\bullet}\ a^{\bullet})\ (\app^{\CS} \circleddollar f \circledast a))
          \end{alignat*}
          \begin{alignat*}{3}
            & \BoolTy^{\bullet} && :{ } && \Ty^{\bullet}\ (\lambda \mmod{F^{\ast}} \mapsto \BoolTy) \\
            & \true^{\bullet} && :{ } && \Tm^{\bullet}\ \BoolTy^{\bullet}\ (\lambda \mmod{F^{\ast}} \mapsto \true) \\
            & \false^{\bullet} && :{ } && \Tm^{\bullet}\ \BoolTy^{\bullet}\ (\lambda \mmod{F^{\ast}} \mapsto \false) \\
            & \elimBool^{\bullet} && :{ } && \forall P\ t\ f\ b\ (P^{\bullet} : \forall x\ (x^{\bullet} : \Tm^{\bullet}\ \BoolTy^{\bullet}\ x) \to \Ty^{\bullet}\ (P \circledast x)) \\
            &&&&& \phantom{\forall} (t^{\bullet} : \Tm^{\bullet}\ (P^{\bullet}\ \true^{\bullet})\ t) (f^{\bullet} : \Tm^{\bullet}\ (P^{\bullet}\ \false^{\bullet})\ f) \\
            &&&&& \to (b^{\bullet} : \Tm^{\bullet}\ \BoolTy^{\bullet}\ b) \to \Tm^{\bullet}\ (P^{\bullet}\ b^{\bullet})\ b
          \end{alignat*}

    \item Satisfying displayed variants of the type-theoretic equations\footnote{Note that these equations are well-typed because of the corresponding equations in $\CS$. As presheaves support equality reflection, we don't have to write transports.} of $\Th_{\Pi,\BoolTy}$.
          \begin{alignat*}{3}
            & \elimBool^{\bullet}\ P^{\bullet}\ t^{\bullet}\ f^{\bullet}\ \true^{\bullet} && ={ } && t^{\bullet} \\
            & \elimBool^{\bullet}\ P^{\bullet}\ t^{\bullet}\ f^{\bullet}\ \false^{\bullet} && ={ } && f^{\bullet}
            \tag*{\lipicsEnd}
          \end{alignat*}
  \end{itemize}
\end{definition}

A displayed model without context extensions has context extensions when for any $A$ and $A^{\bullet}$, the first projection map
\[ (a : \mmod{F^{\ast}} \to \Tm^{\CS}\ (A\ \mmod{F^{\ast}})) \times (a^{\bullet} : \Tm^{\bullet}\ A^{\bullet}\ a) \xrightarrow{\lambda (a,a^{\bullet}) \mapsto a} (\mmod{F^{\ast}} \to \Tm^{\CS}\ (A\ \mmod{F^{\ast}})) \]
has a locally representable domain and preserves context extensions.

In \cref{sec:sections} we give an internal definition of section of displayed models with context extensions.
It is similar to the definition of morphism of models.
The induction principle of the biinitial model $\Init_{\Th_{\Pi,\BoolTy}}$ is the statement that any displayed model with context extensions over $\Init_{\Th_{\Pi,\BoolTy}}$ admits a section.

While (displayed) models without context extensions are not well-behaved, we show that they can be replaced by (displayed) models with context extensions.%
\begin{restatable}{definition}{restateDefFactorization}\label{def:disp_model_factorization}
  A \defemph{factorization} $(\CC \xrightarrow{Y} \CP \xrightarrow{G} \CS, \CS^{\dagger})$ of a global displayed model without context extensions $\CS^{\bullet}$ over $F : \CC \to \CS$ consists of a factorization $\CC \xrightarrow{Y} \CP \xrightarrow{G} \CS$ of $F$ and a displayed model with context extensions $\CS^{\dagger}$ over $G : \CP \to \CS$, such that $Y : \CC \to \CP$ is fully faithful and equipped with bijective actions on displayed types and terms.
  \lipicsEnd{}
\end{restatable}

\begin{restatable}{construction}{restateDispReplace}\label{con:disp_replace_0}
  We construct a factorization $(\CC \xrightarrow{Y} \CP \xrightarrow{G} \CS, \CS^{\dagger})$ of any model without context extensions $\CS^{\bullet}$ over $F : \CC \to \CS$.
\end{restatable}
\begin{proof}[Construction sketch]
  We give the full construction in the appendix.
  We see $\CP$ as analogous to the presheaf category over $\CC$, but in the slice $2$-category $(\CCat / \CS)$.
  Indeed, a generalization of the Yoneda lemma holds for $Y : \CC \to \CP$.
  In particular $Y : \CC \to \CP$ is fully faithful.

  Equivalently, it could be defined as the pullback along $\yo : \CS \to \widehat{\CS}$ of the Artin gluing $\CG \to \widehat{\CS}$ of $F_{\ast} : \widehat{\CS} \to \widehat{\CC}$.

  It is well-known that given a base model $\CC$ of type theory, that model can be extended to the presheaf category $\widehat{\CC}$ in such a way that the Yoneda embedding $\yo : \CC \to \widehat{\CC}$ is a morphism of models with bijective actions on types and terms.
  This is indeed the justification for one of the intended models of two-level type theory~\cite{TwoLevelTypeTheoryAndApplications}.
  This construction does not actually depend on the context extensions in the base model $\CC$.
  The construction of the displayed model $\CS^{\dagger}$ over $G : \CP \to \CS$ is a generalization of this construction to displayed models.
\end{proof}

We now assume that we have a section $S_{0}$ of the displayed model without context extensions $\CS^{\dagger}$ constructed in \cref{con:disp_replace_0}.

In general, we want more than just the section $S_{0}$.
Indeed, if we take a type $A$ of $\CS$ over a context $F\ \Gamma$ for some $\Gamma : \abs{\CC}$, we can apply the action of $S_{0}$ on types to obtain a displayed type $S_{0}^{\Ty}\ A$ of $\CS^{\dagger}$ over $S_{0}\ (F\ \Gamma)$.
We would rather have a displayed type of $\CS^{\bullet}$ over $\Gamma$.
It suffices to have a morphism $\alpha_{\Gamma} : Y\ \Gamma \to S_{0}\ (F\ \Gamma)$.
We can then transport $S_{0}^{\Ty}\ A$ to a displayed type $(S_{0}^{\Ty}\ A)[\alpha_{\Gamma}]$ of $\CS^{\dagger}$ over $Y\ \Gamma$.
Since $Y$ is equipped with a bijective action $Y^{\Ty}$ on displayed types, this provides a displayed type $Y^{\Ty,-1}\ (S_{0}^{\Ty}\ A)[\alpha_{\Gamma}]$ of $\CS^{\bullet}$ over $\Gamma$, as desired.
In general, we want to have a full natural transformation $\alpha : Y \Ra (F \cdot S_{0})$.

It is useful to consider the universal setting under which such a natural transformation is available.
\begin{definition}
  The \defemph{displayed inserter} $\CI(\CS^{\bullet})$ is a category equipped with a functor $I : \CI(\CS^{\bullet}) \to \CC$ and with a natural transformation $\iota : (I \cdot Y) \Ra (I \cdot F \cdot S_{0})$ such that $(\iota \cdot P) = 1_{(I \cdot F)}$.
  It is the final object among such categories: given any other category $\CJ$ with $J : \CJ \to \CC$ and $\beta : (J \cdot Y) \Ra (J \cdot F \cdot S_{0})$ such that $(\bm{\beta} \cdot P) = 1_{(J \cdot S_{0})}$, there exists a unique functor $X : \CJ \to \CI(\CS^{\bullet})$ such that $J = (X \cdot I)$ and $\beta = (X \cdot \alpha)$.
  \lipicsEnd{}
\end{definition}

Internally to $\CPsh^{\CI(\CS^{\bullet})}$, we then have the following operations:
\begin{alignat*}{3}
  & S_{\iota}^{[X]\Ty} && :{ } && \forall \mmod{I^{\ast}} (A : \mmod{F^{\ast}} \to X \to \Ty)\ x\ (x^{\bullet} : X^{\bullet}\ x) \to \Ty^{\bullet}\ (A \circledast x) \\
  & S_{\iota}^{[X]\Tm} && :{ } && \forall \mmod{I^{\ast}}\ A\ (a : \forall \mmod{F^{\ast}}\ x \to \Tm\ (A\ \mmod{F^{\ast}}\ x))\ x\ (x^{\bullet} : X^{\bullet}\ x) \\
  &&&&& \to \Tm^{\bullet}\ (S_{\iota}^{[X]\Ty}\mmod{I^{\ast}}\ A\ x\ x^{\bullet})\ (a \circledast x),
\end{alignat*}
where $X^{\bullet}$ is defined by induction on the telescope $X$.
They preserve all type-theoretic operations:
\begin{alignat*}{1}
  & S_{\iota}^{[X]\Ty}\ \mmod{I^{\ast}}\ (\lambda \mmod{F^{\ast}}\ x \mapsto \Pi\ (A\ \mmod{F^{\ast}}\ x)\ (B\ \mmod{F^{\ast}}\ x))\ x^{\bullet} \\
  & \quad = \Pi^{\bullet}\ (S_{\iota}^{[X]\Ty}\ \mmod{I^{\ast}}\ A\ x^{\bullet})\ (\lambda a^{\bullet} \mapsto S_{\iota}^{[X,A]\Ty}\ \mmod{I^{\ast}}\ B\ (x^{\bullet},a^{\bullet})) \\
  & S_{\iota}^{[X]\Tm}\ \mmod{I^{\ast}}\ (\lambda \mmod{F^{\ast}}\ x \mapsto \app\ (f\ \mmod{F^{\ast}}\ x)\ (a\ \mmod{F^{\ast}}\ x))\ x^{\bullet} \\
  & \quad = \app^{\bullet}\ (S_{\iota}^{[X]\Tm}\ \mmod{I^{\ast}}\ f\ x^{\bullet})\ (S_{\iota}^{[X]\Ty}\ \mmod{I^{\ast}}\ a\ x^{\bullet}) \\
  & S_{\iota}^{[X]\Ty}\ \mmod{I^{\ast}}\ (\lambda \mmod{F^{\ast}}\ x \mapsto \BoolTy)\ x^{\bullet} = \BoolTy^{\bullet} \\
  & S_{\iota}^{[X]\Tm}\ \mmod{I^{\ast}}\ (\lambda \mmod{F^{\ast}}\ x \mapsto \true)\ x^{\bullet} = \true^{\bullet} \\
  & S_{\iota}^{[X]\Tm}\ \mmod{I^{\ast}}\ (\lambda \mmod{F^{\ast}}\ x \mapsto \false)\ x^{\bullet} = \false^{\bullet} \\
  & S_{\iota}^{[X]\Tm}\ \mmod{I^{\ast}}\ (\lambda \mmod{F^{\ast}}\ x \mapsto \elimBool\ (P\ \mmod{F^{\ast}}\ x)\ (t\ \mmod{F^{\ast}}\ x)\ (f\ \mmod{F^{\ast}}\ x)\ (b\ \mmod{F^{\ast}}\ x))\ x^{\bullet} \\
  & \quad = \elimBool^{\bullet}\ (\lambda b^{\bullet} \mapsto S_{\iota}^{[X,\Tm]\Ty}\ \mmod{I^{\ast}}\ P\ (x^{\bullet},b^{\bullet})) \\
  & \phantom{{ }\quad = \elimBool^{\bullet}\ }(S_{\iota}^{[X]\Tm}\ \mmod{I^{\ast}}\ t\ x^{\bullet})\ (S_{\iota}^{[X]\Tm}\ \mmod{I^{\ast}}\ f\ x^{\bullet})\ (S_{\iota}^{[X]\Tm}\ \mmod{I^{\ast}}\ b\ x^{\bullet})
\end{alignat*}

\begin{restatable}{definition}{restateRelSection}\label{def:rel_section}
  A \defemph{relative section} $S_{\alpha}$ of a factorization $(\CC \xrightarrow{Y} \CP \xrightarrow{G} \CS, \CS^{\dagger})$ of a displayed model without context extensions $\CS^{\bullet}$ over $F : \CC \to \CS$ consists of a section $S_{0}$ of the displayed model with context extensions $\CS^{\dagger}$ along with a natural transformation $\alpha : Y \Ra (F \cdot S_{0})$ such that $(\alpha \cdot G) = 1_{F}$, or equivalently with a section $\angles{\alpha} : \CC \to \CI(\CS^{\bullet})$ of $I : \CI(\CS^{\bullet}) \to \CC$.
  \lipicsEnd{}
\end{restatable}

A relative section $S_{\alpha}$ has actions on types and terms, obtained by pulling $S_{\iota}^{[X]\Ty}$ and $S_{\iota}^{[X]\Tm}$ along $\angles{\alpha}$.
\begin{alignat*}{3}
  & S_{\alpha}^{[X]\Ty} && :{ } && \forall (A : \mmod{F^{\ast}} \to X \to \Ty)\ x\ (x^{\bullet} : X^{\bullet}\ x) \to \Ty^{\bullet}\ (A \circledast x) \\
  & S_{\alpha}^{[X]\Tm} && :{ } && \forall A\ (a : \forall \mmod{F^{\ast}}\ x \to \Tm\ (A\ \mmod{F^{\ast}}\ x))\ x\ (x^{\bullet} : X^{\bullet}\ x) \\
  &&&&& \to \Tm^{\bullet}\ (S_{\alpha}^{[X]\Ty}\ A\ x\ x^{\bullet})\ (a \circledast x),
\end{alignat*}

A displayed model without context extension over the biinitial model does not necessarily admit a relative section; this depends on the functor $F : \CC \to \Init_{\Th}$.
Depending on the universal property of $\CC$, we need to provide some additional data in order to construct $\angles{\alpha} : \CC \to \CI(\Init_{\Th}^{\bullet})$.
Thus, we get a different induction principle for every functor $F : \CC \to \Init_{\Th}$ into $\Init_{\Th}$, which we call the induction principle relative to $F$.
We now state several of these relative induction principles, for our example type theory $\Th_{\Pi,\BoolTy}$ and for cubical type theory $\mathsf{CTT}$.

\begin{restatable}[Induction principle relative to $\{\diamond\} \to \Init_{\Th_{\Pi,\BoolTy}}$]{lemma}{restateIndTerminal}\label{lem:ind_terminal}
  Denote by $\{\diamond\}$ the terminal category (which should rather be seen here as the initial category equipped with a terminal object), and consider the functor $F : \{\diamond\} \to \Init_{\Th_{\Pi,\BoolTy}}$ that selects the empty context of $\Init_{\Th_{\Pi,\BoolTy}}$.

  Any global displayed model without context extensions over $F$ admits a relative section.
  \qed{}
\end{restatable}

\begin{definition}
  A \defemph{renaming algebra} over a model $\CS$ of $\Th_{\Pi,\BoolTy}$ is a category $\CR$ with a terminal object, along with a functor $F : \CR \to \CS$ preserving the terminal object, a locally representable dependent presheaf of variables
  \[ \Var^{\CR} : (A : \mmod{F^{\ast}} \to \Ty^{\CS}) \to \SRepPsh^{\CR} \]
  and an action on variables $\var : \forall A\ (a : \Var\ A)\ \mmod{F^{\ast}} \to \Tm^{\CS}\ (A\ \mmod{F^{\ast}})$ that preserves context extensions.

  The category of renamings $\CRen_{\CS}$ over a model $\CS$ is defined as the biinitial renaming algebra over $\CS$.
  We denote the category of renamings of the biinitial model $\Init_{\Th_{\Pi,\BoolTy}}$ by $\CRen$.
\end{definition}

\begin{restatable}[Induction principle relative to $\CRen \to \Init_{\Th_{\Pi,\BoolTy}}$]{lemma}{restateIndRenamings}\label{lem:ind_renamings}
  Let $\Init_{\Th_{\Pi,\BoolTy}}^{\bullet}$ be a global displayed model without context extensions over $F : \CRen \to \Init_{\Th_{\Pi,\BoolTy}}$, along with, internally to $\CI(\CS^{\bullet})$, a global map
  \[ \var^{\bullet} : \forall \mmod{I^{\ast}} (A : \mmod{F^{\ast}} \to \Ty) (a : \Var\ A) \to \Tm^{\bullet}\ (S_{\iota}^{\Ty}\ \mmod{I^{\ast}}\ A)\ (\var\ a). \]

  Then there exists a relative section $S_{\alpha}$ of $\Init_{\Th_{\Pi,\BoolTy}}^{\bullet}$.
  % It satisfies the additional computation rule $S_{\alpha}^{\Tm}\ (\var_{A}\ a) = (\var^{\bullet}\ \mmod{I^{\ast}}\ A\ a)\ekey{-}{\angles{\alpha}^{\ast}I^{\ast}}{\bullet}$.
  \qed{}
\end{restatable}
The relative section also satisfies a computation rule that relates $S_{\alpha}^{\Tm}\ (\var_{A}\ a)$ and $\var^{\bullet}$.

We also state relative induction principles that can be used to prove canonicity and normalization of cubical type theory.
\begin{definition}
  A cubical CwF is a CwF $\CC$ equipped with a locally representable interval presheaf with two endpoints
  \begin{alignat*}{3}
    & \mathbb{I}^{\CC} && :{ } \SRepPsh^{\CC}, \\
    & 0^{\CC}, 1^{\CC} && :{ } \mathbb{I}^{\CC}.
    \tag*{\lipicsEnd}
  \end{alignat*}
\end{definition}

A model of cubical type theory ($\mathsf{CTT}$) is a cubical CwF equipped with some choice of type-theoretic structures, such as $\Pi$-types, path types, glue types, \etc.

\begin{definition}
  A (cartesian) \defemph{cubical algebra} over a model $\CS$ of $\mathsf{CTT}$ is a category $\CC$ with a terminal object, along with a functor $F : \CC \to \CS$ preserving the terminal object, a locally representable interval presheaf $\mathbb{I}^{\CC} : \SRepPsh^{\CC}$ with two endpoints $0^{\CC}, 1^{\CC} : \mathbb{I}^{\CC}$ and an action $\mathsf{int} : \mathbb{I}^{\CC} \to \mmod{F^{\ast}} \to \mathbb{I}^{\CS}$ that preserves context extensions and the endpoints.

  The category of cubes $\square_{\CS}$ over a model $\CS$ is defined as the biinitial cubical algebra over $\CS$.
  We denote by $\square$ the category of cubes of the biinitial model $\Init_{\mathsf{CTT}}$ of cubical type theory.
  \lipicsEnd{}
\end{definition}

\begin{restatable}[Induction principle relative to $\square \to \Init_{\mathsf{CTT}}$]{lemma}{restateIndCube}\label{lem:ind_cubes}
  Let $\Init_{\mathsf{CTT}}^{\bullet}$ be a global displayed model without context extensions over $F : \square \to \Init_{\mathsf{CTT}}$, along with a map
  \[ \mathsf{int}^{\bullet} : (i : \mathbb{I}^{\square}) \to \mathbb{I}^{\bullet}\ (\mathsf{int}\ i) \]
  such that $\mathsf{int}^{\bullet}\ 0^{\square} = 0^{\bullet}$ and $\mathsf{int}^{\bullet}\ 1^{\square} = 1^{\bullet}$.

  Then there exists a relative section $S_{\alpha}$ of $\Init_{\mathsf{CTT}}^{\bullet}$.
  \qed{}
\end{restatable}

\begin{definition}
  A (cartesian) \defemph{cubical atomic algebra} over a model $\CS$ of $\mathsf{CTT}$ is a category $\CC$ with a terminal object, along with a functor $F : \CC \to \CS$ preserving the terminal object and with the structures of a cubical algebra ($\mathbb{I}^{\CC}, 0^{\CC}, 1^{\CC}, \mathsf{int}$) and of a renaming algebra $(\Var^{\CC}, \var)$.

  The category of cubical atomic contexts $\CA_{\square}$ is the biinitial cubical algebra over the biinitial model $\Init_{\mathsf{CTT}}$ of cubical type theory.
  \lipicsEnd{}
\end{definition}

\begin{restatable}[Induction principle relative to $\CA_{\square} \to \Init_{\mathsf{CTT}}$]{lemma}{restateIndAtomicCube}\label{lem:ind_renaming_cubes}
  Let $\Init_{\mathsf{CTT}}^{\bullet}$ be a global displayed model without context extensions over $F : \CA_{\square} \to \Init_{\mathsf{CTT}}$, along with, internally to $\CPsh^{\CI(\Init_{\mathsf{CTT}}^{\bullet})}$, global maps
  \begin{alignat*}{3}
    & \var^{\bullet} && :{ } && \forall \mmod{I^{\ast}}\ (A : \mmod{F^{\ast}} \to \Ty) (a : \Var\ A) \to \Tm^{\bullet}\ (S_{\iota}^{\Ty}\ \mmod{I^{\ast}}\ A)\ (\var\ a), \\
    & \mathsf{int}^{\bullet} && :{ } && \forall \mmod{I^{\ast}}\ (i : \mathbb{I}^{\CA_{\square}}) \to \mathbb{I}^{\bullet}\ (\mathsf{int}\ i),
  \end{alignat*}
  such that $\mathsf{int}^{\bullet}\ \mmod{I^{\ast}}\ 0^{\CA_{\square}} = 0^{\bullet}$ and $\mathsf{int}^{\bullet}\ \mmod{I^{\ast}}\ 1^{\CA_{\square}} = 1^{\bullet}$.

  Then there exists a relative section $S_{\alpha}$ of $\Init_{\mathsf{CTT}}^{\bullet}$.
  \qed{}
\end{restatable}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
