\section{Normalization}\label{sec:normalization}

In this section, we describe a normalization proof for a type theory $\Th_{\UU}$ with a hierarchy of universes indexed by natural numbers, closed under $\Pi$-types and boolean types.
This proof relies on an induction principle relative to $\CRen \to \Init_{\Th_{\UU}}$.

The proof follows the same structure as Coquand normalization proof from~\cite{CoquandNormalization}; it is an algebraic presentation of Normalization by Evaluation (NbE).
There is one important difference between the type theory $\Th_{\UU}$ and the type theory considered in~\cite{CoquandNormalization}.
Our type theory is not cumulative; type-formers at different universe level are distinct.
We believe that proving normalization for a cumulative type theory should be possible in our framework, but getting the details right is tricky and the definitions become very verbose.
These details, such as the precise definition of the normal forms, were omitted from Coquand's proof.
We prove slightly more; in addition to the existence of normal forms, we also prove uniqueness.
Proving uniqueness relies on the computation rules of relative sections.

\subsection{The type theory \texorpdfstring{$\Th_{\UU}$}{T\_{}U}}
We describe the structure of a model of $\Th_{\UU}$ over a category $\CC$, internally to $\CPsh^{\CC}$.

Types and terms are now indexed by universe levels.
\begin{alignat*}{3}
  & \Ty && :{ } && \forall (i : \Nat) \to \SPsh^{\CC} \\
  & \Tm && :{ } && \forall (i : \Nat)\ (A : \Ty_{i}) \to \SRepPsh^{\CC}
\end{alignat*}

We have lifting functions that can be used to move between different universe levels.
\begin{alignat*}{3}
  & \Lift && :{ } && \forall (i : \Nat) \to \Ty_{i} \to \Ty_{i+1} \\
  & \lift && :{ } && \forall (i : \Nat)\ (A : \Ty_{i}) \to \Tm_{i}\ A \simeq \Tm_{i+1}\ (\Lift_{i}\ A)
\end{alignat*}

We have a hierarchy of universes, indexed by universe levels.
The terms of the $i$-th universe are in bijection with the types of level $i$.
\begin{alignat*}{3}
  & \UU && :{ } && \forall (i : \Nat) \to \Ty_{i+1} \\
  & \El && :{ } && \forall (i : \Nat) \to \Tm_{i+1}\ \UU_{i} \simeq \Ty_{i}
\end{alignat*}

Finally, we have $\Pi$-types and boolean numbers types at every universe level.
The motive of the eliminator for booleans can be at any universe level.
\begin{alignat*}{3}
  & \Pi && :{ } && \forall i\ (A : \Ty_{i})\ (B : \Tm\ A \to \Ty_{i}) \to \Ty_{i} \\
  & \app && :{ } && \forall i\ A\ B \to \Tm_{i}\ (\Pi\ A\ B) \simeq ((a : \Tm\ A) \to \Tm\ (B\ a)) \\
  & - && :{ } && \elimBool\ P\ t\ f\ \true = t \\
  & - && :{ } && \elimBool\ P\ t\ f\ \false = f
\end{alignat*}
\begin{alignat*}{3}
  & \BoolTy && :{ } && \forall i \to \Ty_{i} \\
  & \true,\false && :{ } && \forall i \to \Tm_{i}\ \BoolTy \\
  & \elimBool && :{ } && \forall i\ j\ (P : \Tm\ \BoolTy \to \Ty_{j})\ (t : \Tm\ (P\ \true))\ (f : \Tm\ (P\ \false))\ (b : \Tm\ \BoolTy_{i}) \to \Tm\ (P\ b)
\end{alignat*}

We have, as for $\Th_{\Pi,\BoolTy}$, notions of displayed models without context extensions and of relative sections for $\Th_{\UU}$.
The following variant of \cref{lem:ind_renamings} can easily be proven for $\Th_{\UU}$.

\begin{definition}
  A \defemph{renaming algebra} over a model $\CS$ of $\Th_{\UU}$ is a category $\CR$ with a terminal object, along with a functor $F : \CR \to \CS$ preserving the terminal object, a locally representable dependent presheaf of variables
  \[ \Var^{\CR} : \forall i\ (A : \mmod{F^{\ast}} \to \Ty_{i}^{\CS}) \to \SRepPsh^{\CR} \]
  and an action on variables $\var : \forall i\ A\ (a : \Var_{i}\ A)\ \mmod{F^{\ast}} \to \Tm_{i}^{\CS}\ (A\ \mmod{F^{\ast}})$ that preserves context extensions.

  The category of renamings $\CRen_{\CS}$ over a model $\CS$ is defined as the biinitial renaming algebra over $\CS$.
  We denote the category of renamings of the biinitial model $\Init_{\Th_{\UU}}$ by $\CRen$.
  \lipicsEnd{}
\end{definition}

\begin{lemma}[Induction principle relative to $\CRen \to \Init_{\Th_{\UU}}$]\label{lem:ind_renamings_univ}
  Let $\Init_{\Th_{\UU}}^{\bullet}$ be a global displayed model without context extensions over $F : \CRen \to \Init_{\Th_{\UU}}$, along with, internally to $\CI(\CS^{\bullet})$, a global map
  \[ \var^{\bullet} : \forall \mmod{I^{\ast}}\ i\ (A : \mmod{F^{\ast}} \to \Ty_{i}) (a : \Var_{i}\ A) \to \Tm^{\bullet}\ (S_{\iota}^{\Ty}\ \mmod{I^{\ast}}\ A)\ (\var\ a). \]

  Then there exists a relative section $S_{\alpha}$ of $\Init_{\Th_{\UU}}^{\bullet}$.
  It satisfies the additional computation rule
  \[ S_{\alpha}^{\Tm}\ (\var\ a) = (\var^{\bullet}\ \mmod{I^{\ast}})\smkey{\bullet}{\angles{\alpha}^{\ast}I^{\ast}}\ a. \]
  \qed{}
\end{lemma}

\subsection{Normal forms}

The goal of normalization is to prove that every term admits a unique normal form.
We first need to define normal types, normal forms and neutral terms (which correspond to stuck computations).
They are defined, internally to $\CPsh^{\CRen}$, as inductive families indexed by the terms of $\Init_{\Th_{\UU}}$.
\begin{alignat*}{3}
  & \Ne && :{ } && \forall i\ (A : \mmod{F^{\ast}} \to \Ty_{i}) \to (\mmod{F^{\ast}} \to \Tm_{i}\ (A\ \mmod{F^{\ast}})) \to \SPsh^{\CRen} \\
  & \Nf && :{ } && \forall i\ (A : \mmod{F^{\ast}} \to \Ty_{i}) \to (\mmod{F^{\ast}} \to \Tm_{i}\ (A\ \mmod{F^{\ast}})) \to \SPsh^{\CRen} \\
  & \NfTy && :{ } && \forall i \to (\mmod{F^{\ast}} \to \Ty_{i}) \to \SPsh^{\CRen}
\end{alignat*}
An element of $\Ne\ a$ (\resp{} $\Nf\ a$) is a witness of the fact that the term $a$ is a neutral term (\resp{} admits a normal form).
An element of $\NfTy\ A$ is a witness that the type $A$ admits a normal form.

We list below the constructors of these inductive families.
\begin{alignat*}{3}
  % Neutrals
  & \var^{\ne} && :{ } && \forall i\ A\ (a : \Var_{i}\ A) \to \Ne_{i}\ A\ (\var\ a) \\
  & \lift^{-1,\ne} && :{ } && \forall i\ A \to \Ne_{i+1}\ (\Lift_{i}\ A)\ a \to \Ne_{i}\ (\lift^{-1}\ a) \\
  & \app^{\ne} && :{ } && \forall i\ A\ B\ f\ a \to \Ne_{i}\ f \to \Nf_{i}\ a \\
  &&&&& \to \Ne_{i}\ (\app \circleddollar A \circledast B \circledast f \circledast a) \\
  & \elimBool^{\ne} && :{ } && \forall i\ j\ P\ t\ f\ b \to ((m : \Var\ (\lambda \mmod{F^{\ast}} \mapsto \BoolTy_{i})) \to \NfTy_{j}\ (P \circledast \var\ m)) \\
  &&&&& \to \Nf\ t \to \Nf\ f \to \Ne_{i}\ b \to \Ne_{i}\ (\elimBool \circleddollar P \circledast f \circledast t \circledast b) \\
\end{alignat*}
\begin{alignat*}{3}
  % Normal forms
  & \nfty^{\nf} && :{ } && \forall i \to \NfTy_{i}\ A \to \Nf_{i+1}\ (\lambda \mmod{F^{\ast}} \mapsto \UU_{i})\ A \\
  & \ne^{\nf}_\BoolTy && :{ } && \forall i\ a \to \Ne_{i}\ (\lambda \mmod{F^{\ast}} \mapsto \BoolTy_{i})\ a \to \Nf_{i}\ a \\
  & \ne^{\nf}_{\El} && :{ } && \forall i\ A \to \Ne_{i+1}\ (\lambda \mmod{F^{\ast}} \mapsto \UU_{i})\ A \\
  &&&&& \to \Ne_{i}\ (\El \circleddollar A)\ a \to \Nf_{i}\ a \\
  & \lift^{\nf} && :{ } && \forall i\ A \to \Nf_{i}\ A\ a \to \Nf_{i+1}\ (\Lift\ A)\ (\lift\ a) \\
  & \true^{\nf} && :{ } && \forall i \to \Nf_{i}\ \true \\
  & \false^{\nf} && :{ } && \forall i \to \Nf_{i}\ \false \\
  & \lam^{\nf} && :{ } && \forall i\ A\ B\ b \to \NfTy_{i}\ A \to ((a : \Var\ A) \to \NfTy_{i}\ (B \circledast \var\ a)) \\
  &&&&& \to ((a : \Var\ A) \to \Nf_{i}\ (b \circledast \var\ a)) \\
  &&&&& \to \Nf_{i}\ (\lam \circleddollar A \circledast B \circledast b) \\
\end{alignat*}
\begin{alignat*}{3}
  % Normal types
  & \ne^{\nfty}_{\UU} && :{ } && \forall i\ A \to \Ne_{i+1}\ (\lambda\mmod{F^{\ast}} \to \UU_{i})\ A \to \NfTy_{i+1}\ (\El \circleddollar A) \\
  & \UU^{\nfty} && :{ } && \forall i \to \NfTy_{i+1}\ (\lambda\mmod{F^{\ast}} \to \UU_{i}) \\
  & \Lift^{\nfty} && :{ } && \forall i \to \NfTy_{i}\ A \to \NfTy_{i+1}\ (\Lift\ A) \\
  & \BoolTy^{\nfty} && :{ } && \forall i \to \NfTy_{i}\ (\lambda\mmod{F^{\ast}} \to \BoolTy_{i}) \\
  & \Pi^{\nfty} && :{ } && \forall i\ A\ B \to \NfTy_{i}\ A \to ((a : \Var\ A) \to \NfTy_{i}\ (B \circledast \var\ a)) \\
  &&&&& \to \NfTy_{i}\ (\Pi \circleddollar A \circledast B)
\end{alignat*}

The construction of our normalization function will work for any algebra ($\Ne$, $\Nf$, $\NfTy$, $\Ne$, $\Nf$, $\NfTy$, $\dotsc$) with the above signature.
The choice of the initial algebra is only needed to show uniqueness of normal forms in~\cref{lem:stab_norm}.

\subsection{The normalization displayed model}

We now construct a displayed model without context extensions $\Init_{\Th_{\UU}}^{\bullet}$ over $F : \CRen \to \Init_{\Th_{\UU}}$, internally to $\CPsh^{\CRen}$.

A displayed type $A^{\bullet} : \Ty^{\bullet}\ A$ of $\Init_{\Th_{\UU}}^{\bullet}$ over a type $A : \mmod{F^{\ast}} \to \Ty_{i}$ consists of four components $(A^{\bullet}_{\nfty}, A^{\bullet}_{p}, A^{\bullet}_{\ne}, A^{\bullet}_{\nf})$.
\begin{itemize}
  \item $A^{\bullet}_{\nfty} : \NfTy_{i}\ A$ is a witness of the fact that the type $A$ admits a normal form.
  \item $A^{\bullet}_{p} : (\mmod{F^{\ast}} \to \Tm\ (A\ \mmod{F^{\ast}})) \to \SPsh^{\CRen}_{i}$ is a proof-relevant logical predicate over the terms of type $A$, valued in $i$-small presheaves.
  \item $A^{\bullet}_{\ne} : \forall a \to \Ne_{i}\ a \to A^{\bullet}_{p}\ a$ shows that neutral terms satisfy the logical predicate $A^{\bullet}_{p}$.
    The function $A^{\bullet}_{\ne}$ is often called \emph{unquote} or \emph{reflect}.
  \item $A^{\bullet}_{\nf} : \forall a \to A^{\bullet}_{p}\ a \to \Nf_{i}\ a$ shows that the terms that satisfy the logical predicate $A^{\bullet}_{p}$ admit normal forms.
    The function $A^{\bullet}_{\nf}$ is often called \emph{quote} or \emph{reify}.
\end{itemize}

A displayed term $a^{\bullet} : \Tm^{\bullet}\ A^{\bullet}\ a$ of type $A^{\bullet}$ over a term $(a : \mmod{F^{\ast}} \to \Tm\ (A\ \mmod{F^{\ast}}))$ is an inhabitant $a^{\bullet}$ of $A^{\bullet}_{p}\ a$, \ie{} a witness of the fact that $a$ satisfies the logical predicate $A^{\bullet}_{p}$.

The displayed lifted types are defined as follows.
Because the structures of $\Th_{\UU}$ are not strictly preserved by these lifting operations, $\Lift_{i}\ A$ can be seen as a record type with a projection $\lift^{-1}$ and a constructor $\lift$.
This definition would directly extend to $\Sigma$-types or to other record types.
\begin{alignat*}{3}
  & \Lift^{\bullet} && :{ } && \forall i\ A \to \Ty^{\bullet}_{i}\ A \to \Ty^{\bullet}_{i+1}\ (\Lift_{i} \circleddollar A) \\
  & {(\Lift^{\bullet}_{i}\ A^{\bullet})}_{\nfty} && \triangleq{ } && \Lift^{\nfty}\ A^{\bullet}_{\nfty} \\
  & {(\Lift^{\bullet}_{i}\ A^{\bullet})}_{p} && \triangleq{ } && \lambda a \mapsto A^{\bullet}_{p}\ (\lift^{-1} \circleddollar a) \\
  & {(\Lift^{\bullet}_{i}\ A^{\bullet})}_{\ne} && \triangleq{ } && \lambda a_{\ne} \mapsto A^{\bullet}_{\ne}\ (\lift^{-1,\ne}\ a_{\ne}) \\
  & {(\Lift^{\bullet}_{i}\ A^{\bullet})}_{\nf} && \triangleq{ } && \lambda a^{\bullet} \mapsto \lift^{\nf}\ (A^{\bullet}_{\nf}\ a^{\bullet})
\end{alignat*}

The definition of the displayed universes of the normalization displayed model is below.
\begin{alignat*}{3}
  & \UU^{\bullet}_{i} && :{ } && \Ty_{i+1}^{\bullet}\ (\lambda\ \mmod{F^{\ast}} \mapsto \UU_{i}) \\
  & \UU^{\bullet}_{i,\nfty} && \triangleq{ } && \UU^{\nfty}_{i} \\
  & \UU^{\bullet}_{i,p} && \triangleq{ } && \lambda A \mapsto \Ty^{\bullet}_{i}\ (\El \circleddollar A) \\
  & {(\UU^{\bullet}_{i,\ne}\ A\ A_{\ne})}_{\nfty} && \triangleq{ } && \ne^{\nfty}_{\UU}\ A_{\ne} \\
  & {(\UU^{\bullet}_{i,\ne}\ A\ A_{\ne})}_{p} && \triangleq{ } && \lambda a \mapsto \Ne\ a \\
  & {(\UU^{\bullet}_{i,\ne}\ A\ A_{\ne})}_{\ne} && \triangleq{ } && \lambda a_{\ne} \mapsto a_{\ne} \\
  & {(\UU^{\bullet}_{i,\ne}\ A\ A_{\ne})}_{\nf} && \triangleq{ } && \lambda a_{\ne} \mapsto \ne^{\nf}_{\El}\ a_{\ne} \\
  & \UU^{\bullet}_{i,\nf}\ A\ A^{\bullet} && \triangleq{  } && A^{\bullet}_{\nfty}
\end{alignat*}
The most interesting part is the component $\UU^{\bullet}_{i,\ne}$ that constructs a displayed type over any neutral element of the universe; any element of a neutral type is itself neutral.

For $\Pi$-types, the logical predicates are defined in the same way as for canonicity.
\begin{alignat*}{3}
  & {(\Pi^{\bullet}\ A^{\bullet}\ B^{\bullet})}_{\nfty} && \triangleq{ } &&
  \Pi^{\nfty}\ A^{\nfty}\ (\lambda a_{\var} \mapsto \mathsf{let}\ a^{\bullet} = A^{\bullet}_{\ne}\ (\var^{\ne}\ a_{\var})\ \mathsf{in}\ {(B^{\bullet}\ a^{\bullet})}_{\nfty}) \\
  & {(\Pi^{\bullet}\ A^{\bullet}\ B^{\bullet})}_{p}\ A && \triangleq{ } &&
  \forall a\ (a^{\bullet} : A^{\bullet}_{p}) \to {(B^{\bullet}\ a^{\bullet})}_{p} \\
  & {(\Pi^{\bullet}\ A^{\bullet}\ B^{\bullet})}_{\ne}\ f_{\ne} && \triangleq{ } &&
  \lambda a^{\bullet} \mapsto {(B^{\bullet}\ a^{\bullet})}_{\ne}\ (\app^{\ne}\ f_{\ne}\ (A^{\bullet}_{\nf}\ a^{\bullet})) \\
  & {(\Pi^{\bullet}\ A^{\bullet}\ B^{\bullet})}_{\nf}\ f^{\bullet} && \triangleq{ } && \lam^{\nf}\ (\lambda a_{\var} \mapsto \mathsf{let}\ a^{\bullet} = A^{\bullet}_{\ne}\ (\var^{\ne}\ a_{\var})\ \mathsf{in}\ {(B^{\bullet}\ a^{\bullet})}_{\nf}\ (f^{\bullet}\ a^{\bullet}))
\end{alignat*}

For booleans, we define an inductive family $\BoolTy^{\bullet}_{p} : (\mmod{F^{\ast}} \to \Tm\ \BoolTy) \to \SPsh^{\CRen}$ generated by
\begin{alignat*}{3}
  & \true^{\bullet} && :{ } && \BoolTy^{\bullet}_{p}\ (\lambda\ \mmod{F^{\ast}} \mapsto \true) \\
  & \false^{\bullet} && :{ } && \BoolTy^{\bullet}_{p}\ (\lambda\ \mmod{F^{\ast}} \mapsto \false) \\
  & \ne^{\BoolTy^{\bullet}_{p}} && :{ } && \forall b \to \Ne\ b \to \BoolTy^{\bullet}_{p}\ b
\end{alignat*}
This family witnesses the fact that a boolean term is an element of the free bipointed presheaf generated by the neutral boolean terms.
This extends to the following definition of the displayed boolean type in the normalization model.
\begin{alignat*}{3}
  & \BoolTy^{\bullet}_{\nfty} && \triangleq{ } && \BoolTy^{\nfty} \\
  & \BoolTy^{\bullet}_{\ne}\ n_{\ne} && \triangleq{ } && \ne^{\BoolTy^{\bullet}_{p}} \\
  & \BoolTy^{\bullet}_{\nf}\ \true^{\bullet} && \triangleq{ } && \true^{\nf} \\
  & \BoolTy^{\bullet}_{\nf}\ \false^{\bullet} && \triangleq{ } && \false^{\nf} \\
  & \BoolTy^{\bullet}_{\nf}\ (\ne^{\BoolTy^{\bullet}_{p}}\ b_{\ne}) && \triangleq{ } && \ne^{\nf}_{\BoolTy}\ b_{\ne}
\end{alignat*}

The displayed boolean eliminator $\elimBool^{\bullet}$ is defined using the induction principle of $\BoolTy^{\bullet}_{p}$.

\subsection{Normalization}

Given any displayed type $A^{\bullet}$, every variable of type $A$ satisfies the logical predicate $A^{\bullet}_{p}$; we can define
\begin{alignat*}{3}
  & \var^{\bullet} && :{ } && \forall \mmod{I^{\ast}}\ i\ (A : \mmod{F^{\ast}} \to \Ty_{i}) (a : \Var\ A) \to \Tm^{\bullet}\ (S^{\Ty}_{\iota}\ \mmod{I^{\ast}}\ A)\ (\var\ a) \\
  & \var^{\bullet} && \triangleq{ } && \lambda \mmod{I^{\ast}}\ A\ a \mapsto {(S^{\Ty}_{\iota}\ \mmod{I^{\ast}}\ A)}_{\ne}\ (\var^{\ne}\ a)
\end{alignat*}

We can now apply \cref{lem:ind_renamings_univ} to $\Init_{\Th_{\UU}}^{\bullet}$.
We obtain a relative section $S_{\alpha}$ of $\Init_{\Th_{\UU}}^{\bullet}$.

This proves the existence of normal forms, as witnessed by the following normalization function, internally to $\CPsh^{\CRen}$.
\begin{alignat*}{3}
  & \norm && :{ } && \forall i\ (A : \mmod{F^{\ast}} \to \Ty_{i})\ (a : \mmod{F^{\ast}} \to \Tm\ (A\ \mmod{F^{\ast}})) \to \Nf_{i}\ a \\
  & \norm\ A\ a && \triangleq{ } && {(S_{\alpha}^{\Ty}\ A)}_{\nf}\ (S_{\alpha}^{\Tm}\ a)
\end{alignat*}

\subsection{Stability of normalization}

It remains to show the uniqueness of normal forms.
It follows from stability of normalization:%
\begin{lemma}[Internally to $\CPsh^{\CRen}$]\label{lem:stab_norm}
  For every $a_{\ne} : \Ne_{i}\ A\ a$, we have $S_{\alpha}^{\Tm}\ a = {(S_{\alpha}^{\Ty}\ A)}_{\ne}\ a_{\ne}$, and for every $a_{\nf} : \Nf_{i}\ A\ a$, we have ${(S_{\alpha}^{\Ty}\ A)}_{\nf}\ (S_{\alpha}^{\Tm}\ a) = a_{\nf}$.
  Furthermore for every $A_{\nfty} : \NfTy_{i}\ A$, we have ${(S_{\alpha}^{\Ty}\ A)}_{\nfty} = A_{\nfty}$.
\end{lemma}
\begin{proof}
  This lemma is proven by induction on $\Ne$, $\Nf$, $\NfTy$.

  We only list some of the cases.
  \begin{description}
    \item[$\var^{\ne}\ a : \Ne\ (\var\ A\ a)$]
          \begin{alignat*}{1}
            & S_{\alpha}^{\Tm}\ (\var\ A\ a) \\
            & \quad { }= {(S_{\alpha}^{\Ty}\ A)}_{\ne}\ (\var^{\ne}\ a)
            \tag*{(computation rule of $S_{\alpha}$)}
          \end{alignat*}
    \item[$\app^{\ne}\ f_{\ne}\ a_{\nf} : \Ne\ (\app \circleddollar A \circledast B \circledast f \circledast a)$]
          \begin{alignat*}{1}
            & S_{\alpha}^{\Tm}\ (\app \circleddollar f \circledast a) \\
            & \quad { }= \app^{\bullet}\ (S_{\alpha}^{\Tm}\ f)\ (S_{\alpha}^{\Tm}\ a)
            \tag*{(computation rule of $S_{\alpha}$)} \\
            & \quad { }= (S_{\alpha}^{\Tm}\ f)\ (S_{\alpha}^{\Tm}\ a)
            \tag*{(definition of $\app^{\bullet}$)} \\
            & \quad { }= ({(S_{\alpha}^{\Ty}\ (\Pi\ A\ B))}_{\ne}\ f_{\ne})\ (S_{\alpha}^{\Tm}\ a)
            \tag*{(induction hypothesis for $f_{\ne}$)} \\
            & \quad { }= {(\Pi^{\bullet}\ (S_{\alpha}^{\Ty}\ A)\ (S_{\alpha}^{[\Tm]\Ty}\ B))}_{\ne}\ f_{\ne}\ (S_{\alpha}^{\Tm}\ a)
            \tag*{(computation rule of $S_{\alpha}$)} \\
            & \quad { }= {((S_{\alpha}^{[\Tm]\Ty}\ B)\ (S_{\alpha}^{\Tm}\ a))}_{\ne}\ {(\app^{\ne}\ f_{\ne}\ ({(S_{\alpha}^{\Ty}\ A)}_{\nf}\ (S_{\alpha}^{\Tm}\ a)))}
            \tag*{(definition of $\Pi^{\bullet}$)} \\
            & \quad { }= {(S_{\alpha}^{\Ty}\ (B \circledast a))}_{\ne}\ (\app^{\ne}\ f_{\ne}\ a_{\nf})
            \tag*{(induction hypothesis for $a_{\nf}$)}
          \end{alignat*}
    \item[$\lam^{\nf}\ b_{\nf} : \Nf\ (\lam \circleddollar \{A\} \circledast \{B\} \circledast b)$]
          \begin{alignat*}{1}
            & {(S_{\alpha}^{\Ty}\ (\Pi\ A\ B))}_{\nf}\ (S_{\alpha}^{\Tm}\ (\lam \circleddollar b)) \\
            & \quad { }= {(\Pi^{\bullet}\ (S_{\alpha}^{\Ty}\ A)\ (S_{\alpha}^{[\Tm]\Ty}\ B))}_{\nf}\ (\lam^{\bullet}\ (S_{\alpha}^{[\Tm]\Tm}\ b))
            \tag*{(computation rule of $S_{\alpha}$)} \\
            & \quad { }= {(\Pi^{\bullet}\ (S_{\alpha}^{\Ty}\ A)\ (S_{\alpha}^{[\Tm]\Ty}\ B))}_{\nf}\ (\lambda\ a^{\bullet} \mapsto (S_{\alpha}^{[\Tm]\Tm}\ b)\ a^{\bullet})
            \tag*{(definition of $\lam^{\bullet}$)} \\
            & \quad { }= \lam^{\nf}\ (\lambda\ a_{\var} \mapsto \mathsf{let}\ a^{\bullet} = {(S_{\alpha}^{\Ty}\ A)}_{\ne}\ (\var^{\ne}\ a_{\var})\ \mathsf{in}\ {(S_{\alpha}^{[\Tm]\Ty}\ B\ a^{\bullet})}_{\nf}\ (S_{\alpha}^{[\Tm]\Tm}\ b\ a^{\bullet}))
            \tag*{(definition of $\Pi^{\bullet}$)} \\
            & \quad { }= \lam^{\nf}\ (\lambda\ a_{\var} \mapsto \mathsf{let}\ a^{\bullet} = S_{\alpha}^{\Tm}\ (\var\ a_{\var})\ \mathsf{in}\ {(S_{\alpha}^{[\Tm]\Ty}\ B\ a^{\bullet})}_{\nf}\ (S_{\alpha}^{[\Tm]\Tm}\ b\ a^{\bullet}))
            \tag*{(computation rule of $S_{\alpha}$)} \\
            & \quad { }= \lam^{\nf}\ (\lambda\ a_{\var} \mapsto {(S_{\alpha}^{\Ty}\ (B\ \circledast \var\ a_{\var}))}_{\nf}\ (S_{\alpha}^{\Tm}\ (b\ \circledast \var\ a_{\var})))
            \tag*{(computation rule of $S_{\alpha}$)} \\
            & \quad { }= \lam^{\nf}\ (\lambda\ a_{\var} \mapsto b_{\nf}\ a_{\var})
            \tag*{(induction hypothesis for $b_{\nf}$)} \\
            & \quad { }= \lam^{\nf}\ b_{\nf}
          \end{alignat*}
    \item[$\Pi^{\nfty}\ A_{\nfty}\ B_{\nfty} : \NfTy\ (\Pi \circleddollar A \circledast B)$]
          \begin{alignat*}{1}
            & {(S_{\alpha}^{\Ty}\ (\Pi \circleddollar A \circledast B))}_{\nfty} \\
            & \quad { }= {(\Pi^{\bullet}\ (S_{\alpha}^{\Ty}\ A)\ (S_{\alpha}^{[\Tm]\Ty}\ B))}_{\nfty}
            \tag*{(computation rule of $S_{\alpha}$)} \\
            & \quad { }= \Pi^{\nfty}\ {(S_{\alpha}^{\Ty}\ A)}_{\nfty}\ (\lambda a_{\var} \mapsto \mathsf{let}\ a^{\bullet} = {(S_{\alpha}^{\Ty}\ A)}_{\ne}\ (\var^{\ne}\ a_{\var})\ \mathsf{in}\ {(S_{\alpha}^{[\Tm]\Ty}\ B\ a^{\bullet})}_{\nfty})
            \tag*{(definition of $\Pi^{\bullet}$)} \\
            & \quad { }= \Pi^{\nfty}\ {(S_{\alpha}^{\Ty}\ A)}_{\nfty}\ (\lambda a_{\var} \mapsto \mathsf{let}\ a^{\bullet} = s^{\Tm}_{\alpha}\ (\var\ a_{\var})\ \mathsf{in}\ {(S_{\alpha}^{[\Tm]\Ty}\ B\ a^{\bullet})}_{\nfty})
            \tag*{(computation rule of $S_{\alpha}$)} \\
            & \quad { }= \Pi^{\nfty}\ {(S_{\alpha}^{\Ty}\ A)}_{\nfty}\ (\lambda a_{\var} \mapsto {(S_{\alpha}^{\Ty}\ (B\ \circledast \var\ a_{\var}))}_{\nfty})
            \tag*{(computation rule of $S_{\alpha}$)} \\
            & \quad { }= \Pi^{\nfty}\ {(S_{\alpha}^{\Ty}\ A)}_{\nfty}\ (\lambda a_{\var} \mapsto B_{\nfty}\ a_{\var})
            \tag*{(induction hypothesis for $B_{\nfty}$)} \\
            & \quad { }= \Pi^{\nfty}\ A_{\nfty}\ B_{\nfty}
            \tag*{(induction hypothesis for $A_{\nfty}$)} \\
          \end{alignat*}
  \end{description}
\end{proof}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
